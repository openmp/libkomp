/*
 * kmp_hws.cpp -- machine management
 */


#include "kmp_hws.h"
#include "kmp_queues.h"
#include <inttypes.h>

#if KMP_OS_DARWIN
#include <sys/types.h>
#include <sys/sysctl.h>
#endif

//TODO GUARD this whole file

MachineInfo machine_info;

MachineInfo::MachineInfo() {
}

void MachineInfo::init() {
  if (init_done)
    return;
  init_done = 1;

  KA_TRACE( 20, ("MachineInfo init\n"));
  cpu2node = (unsigned int*)__kmp_allocate( __kmp_xproc* sizeof(unsigned int) );

  places_count[KMP_LEVEL_THREAD] = __kmp_xproc;
  for (int i = 0; i < places_count[KMP_LEVEL_THREAD]; i++) {
#if LIBOMP_USE_NUMA
    cpu2node[i] = numa_node_of_cpu(i); 
#else
    cpu2node[i] = 0;
#endif
  }

  dram_size = 0;
#if KMP_OS_LINUX
  L3_size = sysconf(_SC_LEVEL3_CACHE_SIZE);
#elif KMP_OS_DARWIN
  sysctlbyname("hw.l3cachesize", NULL, &L3_size, NULL, 0);
#else
  L3_size = 0;
#endif
#if LIBOMP_USE_EXTSCHED_MEM
  dram_consumption = 0;
  dram_real_consumption = 0;
  dram_limit = (size_t)-1;
  char* limit = getenv("KAAPI_SCHED_MEM_LIMIT");
  if (limit !=0)
  {
    size_t l = atoll(limit);
    if (errno ==0)
      dram_limit = l;
    else {
      fprintf(stderr,"*** conversion error: value defined by 'KAAPI_SCHED_MEM_LIMIT' not used\n");
    }
    fprintf(stdout,"Set limit using 'KAAPI_SCHED_MEM_LIMIT' to %" PRIu64"\n",(uint64_t)dram_limit);
  }
#endif

#if LIBOMP_USE_NUMA
  numa_nodes_count = numa_num_configured_nodes();
  places_count[KMP_LEVEL_NUMA] = numa_nodes_count;
  numa_node_size = (size_t*)__kmp_allocate( numa_nodes_count* sizeof(size_t) );

#if LIBOMP_USE_EXTSCHED_MEM
  __kmp_init_bootstrap_lock( &lock_dram );
  L3_limit = (size_t*)__kmp_allocate( numa_nodes_count* sizeof(size_t) );
  L3_consumption = (size_t*)__kmp_allocate( numa_nodes_count* sizeof(size_t) );
#endif
  for (int i = 0; i < numa_nodes_count; i++) {
   dram_size += numa_node_size[i] = numa_node_size64(i,0);
#if LIBOMP_USE_EXTSCHED_MEM
   L3_limit[i] = (size_t)-1;
   L3_consumption[i] = 0;
#endif
  }
#endif
}

MachineInfo::~MachineInfo() {
  if (init_done) {
    KA_TRACE( 20, ("MachineInfo fini\n"));
  } else {
    KA_TRACE( 20, ("Fini not init\n"));
  }
}

extern "C"
void __kmp_init_lib_use_affinity(void)
{
  static int is_init = 0;
  if (is_init) return;
  is_init = 1;
  machine_info.init();
}
