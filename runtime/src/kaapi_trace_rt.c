/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** fabien.lementec@gmail.com 
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_trace.h"
#include "kaapi_impl.h"

#if defined(__cplusplus)
extern "C" {
#endif


/** Human readable name of events
*/
const char* kaapi_event_name[]
= {
/* 0 */  "K-ProcStart",
/* 1 */  "K-ProcEnd",
/* 2 */  "TaskBegin",
/* 3 */  "TaskEnd",
/* 4 */  "Dependency",
/* 5 */  "Access",
/* 6 */  "DagCompBegin",
/* 7 */  "DagCompEnd",
/* 8 */  0,
/* 9 */  0,
/*10 */  "IdleBeg",
/*11 */  "IdleEnd",
/*12 */  0,
/*13 */  0,
/*14 */  0,
/*15 */  "SuspendBegin",
/*16 */  "SuspendEnd",
/*17 */  "RequestBeg",
/*18 */  "RequestEnd",
/*19 */  "StealOp",
/*20 */  "StealAggrBegin",
/*21 */  "StealAggrBegin",
/*22 */  "H2HBegin",
/*23 */  "H2HEnd",
/*24 */  "H2DBegin",
/*25 */  "H2DEnd",
/*26 */  "D2HBegin",
/*27 */  "D2HEnd",
/*28 */  "D2DBegin",
/*29 */  "D2DEnd",
/*30 */  "KernelBegin",
/*31 */  "KernelEnd",
/*32 */  "ParallelBegin",
/*33 */  "ParallelEnd",
/*34 */  "TaskWaitBegin",
/*35 */  "TaskWaitEnd",
/*36 */  "TaskGroupBegin",
/*37 */  "TaskGroupEnd",
/*38 */  "PerfCounter",
/*39 */  "TaskPerfCntr",
/*40 */  "LockBegin",
/*41 */  "LockEnd",
/*42 */  "YieldBeg",
/*43 */  "YieldEnd",
/*44 */  "BarrierBegin",
/*45 */  "BarrierEnd",
/*46 */  "TaskSteal",
/*47 */  "LoopBegin",
/*48 */  "LoopEnd",
/*49 */  "LoopNext",
/*50 */  "ThreadStealOp",
/*51 */  "ThreadState",
/*52 */  "Energy",
/*53 */  "Watt",
/*54 */  "LoopMData",
/*55 */  "TaskAttr",
/*56 */  "Uncore"
};


/* Meta data about performance counter
*/
kaapi_perfctr_info_t kaapi_perfctr_info[KAAPI_PERF_ID_MAX] = {
/* 0 */  { "Work",        "WORK", "", 0, 1, 0, 0, 0, 0 },
/* 1 */  { "Tidle",       "TIDLE","", 0, 1, 0, 0, 0, 0 },
/* 2 */  { "Tinf",        "TINF", "", 0, 1, 1, 0, 0, 0 },
/* 3 */  { "Time",        "TIME", "", 0, 1, 1, 0, 0, 0 },
/* 4 */  { "TaskSpawn",   "TASKSPAWN", "", 0, 0, 0, 0, 0, 0 },
/* 5 */  { "TaskExec",    "TASKEXEC", "", 0, 0, 0, 0, 0, 0 },
/* 6 */  { "ConflictPop", 0, "", 0, 0, 0, 0, 0, 0 },
/* 7 */  { "StealReqOk",  "STEALOK", "", 0, 0, 0, 0, 0, 0 },
/* 8 */  { "StealReq",    "STEALREQ", "", 0, 0, 0, 0, 0, 0 },
/* 9 */  { "StealOp",     "STEALOP", "", 0, 0, 0, 0, 0, 0 },
/* 10 */ { "#Steal input",  0, "<not implemented>", 0, 0, 0, 0, 0, 0 },
/* 11 */ { "TaskSteal",   "TASKSTEAL", "", 0, 0, 0, 0, 0, 0 },
/* 12 */ { "Sync",        "SYNC", "", 0, 0, 0, 0, 0, 0 },
/* 13 */ { "Energy",      "ENERGY", "Total Energy", 0, 0, 0, 0, 0, 0},
/* 14 */ { "Watt",        "WATT",   "Total Watt", 0, 0, 0, 0, 0, 0},
/* 15 */ { "",    0, "", 0, 0, 0, 0, 0, 0},
/* 16 */ { "",    0, "", 0, 0, 0, 0, 0, 0},
/* 17 */ { "#Task offload", 0, "", 0, 0, 0, 0, 0, 0 },
/* 18 */ { "Cache Hit",    "CACHE_HIT", "", 0, 0, 0, 0, 0, 0 },
/* 19 */ { "Cache Miss",   "CACHE_MISS", "", 0, 0, 0, 0, 0, 0 },
/* 20 */ { "Stack size",   "STACKSIZE", "", 0, 0, 0, 0, 0, 0 },
/* 21 */ { "Tdfgbuild",    "DFGBUILD",  "", 0, 1, 0, 0, 0 },
/* 22 */ { "Tsyncdfg", 0,  "", 0, 1, 0, 0, 0 },
/* 23 */ { "Tinitdfg", 0,  "", 0, 1, 0, 0, 0 },
/* 24 */ { "LocalRead",   "LOCAL_READ", "Number of local read parameters to a task", 0, 0, 0, 0, 0, 0 },
/* 25 */ { "LocalWrite",  "LOCAL_WRITE", "", 0, 0, 0, 0, 0, 0 },
/* 26 */ { "RemoteRead",  "REMOTE_READ", "", 0, 0, 0, 0, 0, 0 },
/* 27 */ { "RemoteWrite", "REMOTE_WRITE", "", 0, 0, 0, 0, 0, 0 },
/* 28 */ { "ParallelReg",     "PARALLELREG", "OpenMP support. Count number of parallel regions", 0, 0, 0, 0, 0, 0 },
/* 29 */ { "LockCnt",         "LOCK", "OpenMP support. Count number of calls to omp_set_lock or omp_set_nested_lock", 0, 0, 0, 0, 0, 0 },
/* 30 */ { "BarrierCnt",      "BARRIER", "OpenMP support. Count number of barrier", 0, 0, 0, 0, 0, 0 },
/* 31 */ { "TaskyieldCnt",    "YIELD", "OpenMP support. Count number of taskyield", 0, 0, 0, 0, 0, 0 },
/* 32 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 33 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 34 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 35 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 36 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 37 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 38 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 39 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 40 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 41 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 42 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 43 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 44 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 45 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 46 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 47 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 48 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 49 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 50 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 51 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 52 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 53 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 54 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 55 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 56 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 57 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 58 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 59 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 60 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 61 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 62 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 63 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
};


/*
*/
const char* kaapi_tracelib_perfid_to_name(kaapi_perf_id_t id)
{
  kaapi_assert_debug( (0 <= id) && (id <KAAPI_PERF_ID_MAX) );
  return kaapi_perfctr_info[id].name;
}

#if defined(__cplusplus)
}
#endif

