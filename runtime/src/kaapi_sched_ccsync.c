/*
** xkaapi
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kmp.h"
#include "kaapi_wsprotocol.h"
#include "kaapi_atomic.h"

#if LIBOMP_USE_NUMA
#include <numa.h>
#include <numaif.h>
#ifndef _GNU_SOURCE
#    define _GNU_SOURCE
#endif
#include <sched.h> /* sched_getcpu */
#endif




/* ============================= REQUEST ============================ */
/** \ingroup WS
    Opcode for different kind of request on a task'queue
*/
typedef enum {
  KAAPI_REQUEST_OP_PUSH,
  KAAPI_REQUEST_OP_PUSH_REMOTE,
  KAAPI_REQUEST_OP_PUSHLIST,
  KAAPI_REQUEST_OP_POP,
  KAAPI_REQUEST_OP_STEAL
} kaapi_request_op_t;

/** Private status of request
    \ingroup WS
    
    The protocol to steal work is very simple:
    1- the thief that want to post a request should provide:
      - an status to indicate the completion of the request (kaapi_atomic_t)
      - a frame where to store theft tasks
    2. The post method initialize fields, do a write barrier and set the status to
    KAAPI_REQUEST_S_POSTED
    3. A victim thread that replies must fill the kaapi_tasksteal_arg_t and
    call kaapi_request_replytask with the status of the steal request: 
    either KAAPI_REQUEST_S_OK in case of success, else KAAPI_REQUEST_S_NOK.
    If the victim want to change kind of reply, it can change the body of the
    task. The pre-allocated arguments for the task is currently of size 
    sizeof(kaapi_tasksteal_arg_t).
*/
/* Request status 
*/
typedef enum kaapi_request_status_t {
  KAAPI_REQUEST_S_INIT     = 0,
  KAAPI_REQUEST_S_NOK      = 1,
  KAAPI_REQUEST_S_OK       = 2,
  KAAPI_REQUEST_S_STOP     = 3,  /* stop to steal, return to upper level */
  KAAPI_REQUEST_S_ERROR    = 4,
  KAAPI_REQUEST_S_POSTED   = 5   /* asynchronous request */
} kaapi_request_status_t;


/** \ingroup WS
    Common header to all requests.
    WARNING these fields must appears AT THE BEGINING of the sub
    request data structure (missing C++ class heritage !)
*/
typedef struct kaapi_header_request_t {
  kaapi_request_op_t            op;             /* the op to apply (on a place) */
  kaapi_wsqueue_t*              queue;          /* queue to reply answer */
  int                           ident;          /* system wide id who is emetting the request */
  int                           mask_arch;      /* accepted arch */
  int                           status;         /* request status */
} kaapi_header_request_t;

/** \ingroup WS
    A steal request.
    First fields are those ofkaapi_header_request_t
*/
typedef struct kaapi_steal_request_t {
  kaapi_request_op_t            op;
  kaapi_wsqueue_t*              queue;          /* queue to reply answer */
  int                           ident;          /* system wide id who is emetting the request */
  int                           mask_arch;      /* accepted arch */
  int                           status;         /* request status */
  kaapi_task_t*                 task;           /* where to store theft tasks/data */
  uintptr_t                     victim;         /* victim: used only to record trace */
  uintptr_t                     serial;         /* serial number: used only to record trace */
} kaapi_steal_request_t;


/** \ingroup WS
    A pop request
*/
typedef struct kaapi_pop_request_t {
  kaapi_request_op_t            op;
  kaapi_wsqueue_t*              queue;          /* queue to reply answer */
  int                           ident;          /* system wide id who is emetting the request */
  int                           mask_arch;      /* accepted arch */
  int                           status;         /* request status */
  kaapi_task_t*                 task;           /* poped */
} kaapi_pop_request_t;


/** \ingroup WS
    Arg for push request
*/
typedef struct kaapi_push_request_t {
  kaapi_request_op_t            op;
  kaapi_wsqueue_t*              queue;          /* queue to reply answer */
  int                           ident;          /* system wide id who is emetting the request */
  int                           mask_arch;      /* accepted arch */
  int                           status;         /* request status */
  kaapi_task_t*                 task;           /* to push */
} kaapi_push_request_t;


#if LIBOMP_USE_LINKED_DEQUEUE
/** \ingroup WS
    Arg for push request
*/
typedef struct kaapi_pushlist_request_t {
  kaapi_request_op_t            op;
  kaapi_wsqueue_t*              queue;          /* queue to reply answer */
  int                           ident;          /* system wide id who is emetting the request */
  int                           mask_arch;      /* accepted arch */
  int                           status;         /* request status */
  kaapi_wsqueue_t*              list;           /* linked list to push */
  int                           size;           /* size of list */
} kaapi_pushlist_request_t;
#endif

/** \ingroup WS
    Request emitted to get work.
    This data structure is pass in parameter of the splitter function.
    On return, the receiver that emits the request will retreive task(s) in the frame.
*/
typedef union kaapi_request_t {
    kaapi_header_request_t   header;
    kaapi_steal_request_t    steal_a;
    kaapi_pop_request_t      pop_a;
    kaapi_push_request_t     push_a;
#if LIBOMP_USE_LINKED_DEQUEUE
    kaapi_pushlist_request_t push_l;
#endif
} kaapi_request_t;


/*
*/
typedef struct kaapi_place_group_operation_t {
  struct kaapi_request_node_t* head;
  struct kaapi_request_node_t* tail;
} kaapi_place_group_operation_t;


/* ============================= INTERFACE ============================ */

/** Return the request status
  \param pksr kaapi_request_t
  \retval KAAPI_REQUEST_S_OK sucessfull steal operation
  \retval KAAPI_REQUEST_S_NOK steal request has failed
  \retval KAAPI_REQUEST_S_ERROR error during steal request processing
*/
static inline int kaapi_request_get_status( kaapi_request_t* kr )
{ return kr->header.status; }

/** Return true iff the request has been posted
  \param kr kaapi_request_t
  \retval KAAPI_REQUEST_S_OK sucessfull steal operation
  \retval KAAPI_REQUEST_S_NOK steal request has failed
  \retval KAAPI_REQUEST_S_ERROR steal request has failed to be posted because the victim refused request
*/
static inline int kaapi_request_test( kaapi_request_t* kr )
{ return kaapi_request_get_status(kr) != KAAPI_REQUEST_S_POSTED; }

/** Return the data associated with the reply
  \param kr kaapi_request_t
*/
static inline void kaapi_request_syncdata( kaapi_request_t* kr ) 
{ 
  KMP_MB();
}


/* Push a request to a queue 
*/
extern kaapi_request_t* kaapi_sched_ccsync_post_request (
  kaapi_wsqueue_t* queue
);

extern int kaapi_request_ccsync_reply
( 
  kaapi_request_t*        request,
  int                     value
);
extern int kaapi_sched_ccsync_commit_request
(
  kaapi_wsqueue_t* queue,
  kaapi_request_t* req
);
extern int kaapi_sched_ccsync_commit_request_async
(
  struct kaapi_place_group_operation_t* kpgo,
  kaapi_wsqueue_t* queue,
  kaapi_request_t* req
);

extern int kaapi_listrequest_ccsync_iterator_empty(
  struct kaapi_listrequest_iterator_t* lri
);
extern kaapi_steal_request_t* kaapi_listrequest_ccsync_iterator_get(
  struct kaapi_listrequest_iterator_t* lri
);
extern kaapi_steal_request_t* kaapi_listrequest_ccsync_iterator_next(
  struct kaapi_listrequest_iterator_t* lri
);
extern int kaapi_listrequest_ccsync_iterator_count(
  struct kaapi_listrequest_iterator_t* lri
);



#if LIBOMP_USE_THE_AGGREGATION
#define NANOSLEEP 0
#if NANOSLEEP
#if !defined(_XOPEN_SOURCE)
#  define _XOPEN_SOURCE 600
#endif
#include <time.h>
#endif
#endif


#define KAAPI_REORDER_LIST 1

/* ========================================================================= */
/* This is an implementation of the CC_Sync algorithm                        */
/* ========================================================================= */

#define KAAPI_CACHE_LINE 64

#if LIBOMP_USE_THE_AGGREGATION

/** This is the new implementation of the combining steal request as proposed by Fatourou, CC_sync
    algorithm. The tail is stored in the team structure.
*/
typedef struct kaapi_request_node_t {
  kaapi_request_t              req;
  struct kaapi_request_node_t* volatile next;
  int volatile                 wait;
  int volatile                 completed;
  struct kaapi_request_node_t* volatile nextNode; /* used to pass data between post and commit */
  struct kaapi_request_node_t* pgo_next;
} kaapi_request_node_t __attribute__((aligned(KAAPI_CACHE_LINE)));


/* structure to retreive request object */
typedef struct kaapi_listrequest_t {
  struct kaapi_request_node_t* tail;
} kaapi_listrequest_t __attribute__((aligned (KAAPI_CACHE_LINE)));


/* The most important structure to iterate over the list of requests
*/
typedef struct kaapi_listrequest_iterator_t {
  int                                 count;
  kaapi_request_node_t**              curr;
} kaapi_listrequest_iterator_t;

#endif

/*
*/
int kaapi_request_ccsync_reply
( 
  kaapi_request_t*        request,
  int                     value
)
{
  request->header.status = value;
#if LIBOMP_USE_THE_AGGREGATION
  kaapi_request_node_t* node = (kaapi_request_node_t*)request;
  node->completed = 1;
  node->wait      = 0;
#endif
  return 0;
}


/* return !=0 iff the range is empty
*/
int kaapi_listrequest_ccsync_iterator_empty(
  kaapi_listrequest_iterator_t* lri
)
{
#if LIBOMP_USE_THE_AGGREGATION
  return (lri->count ==0);
#else
  return 1;
#endif
}


/* get the first request of the range. range iterator should have been initialized by kaapi_listrequest_iterator_init 
*/
kaapi_steal_request_t* kaapi_listrequest_ccsync_iterator_get(
  kaapi_listrequest_iterator_t* lri
)
{ 
#if LIBOMP_USE_THE_AGGREGATION
  return (kaapi_steal_request_t*)*lri->curr;
#else
  return 0;
#endif
}


/* return the next entry in the request. return 0 if the range is empty.
*/
kaapi_steal_request_t* kaapi_listrequest_ccsync_iterator_next(
  kaapi_listrequest_iterator_t* lri
)
{
#if LIBOMP_USE_THE_AGGREGATION
  if (lri->count <=0) return 0;
  --lri->count;
  ++lri->curr;
  return (kaapi_steal_request_t*)*lri->curr;
#else
  return 0;
#endif
}


/*
*/
int kaapi_listrequest_ccsync_iterator_count(
  kaapi_listrequest_iterator_t* lri
)
{
#if LIBOMP_USE_THE_AGGREGATION
  return lri->count;
#else
  return 0;
#endif
}

/* no concurrency here: always called before starting threads */
int kaapi_wsqueue_init(kaapi_wsqueue_t* queue, size_t size, int numa_node)
{
#if LIBOMP_USE_LINKED_DEQUEUE
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_init_bootstrap_lock( &queue->deque_lock );
#endif
  queue->deque_H = 0;
  queue->deque_T = 0;
  queue->deque_size = 0;
  queue->numa_node = numa_node;
#else // LIBOMP_USE_LINKED_DEQUEUE
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_init_bootstrap_lock( &queue->deque_lock_owner );
  __kmp_init_bootstrap_lock( &queue->deque_lock );
#endif
  queue->deque_H = size/2;
  queue->deque_T = size/2;
  queue->numa_node = numa_node;
  queue->deque   = (kaapi_task_t **)
#if LIBOMP_USE_AFFINITY
    (numa_node != -1 ?
        numa_alloc_onnode( size * sizeof(kaapi_task_t *), numa_node)
      : numa_alloc_local( size * sizeof(kaapi_task_t *))
    );
#else
    __kmp_allocate( size * sizeof(kaapi_task_t *));
#endif
  queue->deque_size = (uint32_t)size;
#endif // LIBOMP_USE_LINKED_DEQUEUE

#if LIBOMP_USE_THE_AGGREGATION
  /* first non blocked node ! */
  kaapi_request_node_t* tail = (kaapi_request_node_t*)
#if LIBOMP_USE_AFFINITY
      numa_alloc_local( sizeof(kaapi_request_node_t));
#else
      __kmp_allocate( sizeof(kaapi_request_node_t));
#endif
  tail->wait = 0;
  tail->completed = 0;
  tail->next = 0;
  tail->pgo_next = 0;
  queue->tail = tail;
#endif
  return 0;
}

/* no concurrency here: always called before starting threads */
int kaapi_wsqueue_realloc(kaapi_wsqueue_t* queue )
{
#if LIBOMP_USE_LINKED_DEQUEUE
    // never realloc queue
    return 0;
#else // #if LIBOMP_USE_LINKED_DEQUEUE

    kmp_int32 size = queue->deque_size;
    kmp_int32 new_size = 2 * size;
    kaapi_task_t ** new_deque;

    int i;
    if (queue->deque_H == queue->deque_T)
    {
      queue->deque_H = queue->deque_T = size/2;
      return 0;
    }
    else {
      new_deque = (kaapi_task_t **)
#if LIBOMP_USE_AFFINITY
        (queue->numa_node != -1 ?
            numa_alloc_onnode( new_size * sizeof(kaapi_task_t *), queue->numa_node)
          : numa_alloc_local( new_size * sizeof(kaapi_task_t *))
        );
#else
        __kmp_allocate( new_size * sizeof(kaapi_task_t *));
#endif
      for ( i = 0; i < queue->deque_T; ++i )
         new_deque[i] = queue->deque[i];
    }
#if LIBOMP_USE_AFFINITY
    numa_free((void*)queue->deque, size );
#else
    __kmp_free((void*)queue->deque);
#endif
    queue->deque = new_deque;
    queue->deque_size = new_size;
#endif
    return 0;
}


/* */
static int kaapi_wsqueue_realloc_push_remote(kaapi_wsqueue_t* queue )
{
#if LIBOMP_USE_LINKED_DEQUEUE
    return 0;
#else // #if LIBOMP_USE_LINKED_DEQUEUE
    kmp_int32 size = queue->deque_size;
    kmp_int32 new_size = 2 * size;
    kaapi_task_t ** new_deque;

    int i, shift = size;
    new_deque = (kaapi_task_t **)
#if LIBOMP_USE_AFFINITY
      (queue->numa_node != -1 ?
          numa_alloc_onnode( new_size * sizeof(kaapi_task_t *), queue->numa_node)
        : numa_alloc_local( new_size * sizeof(kaapi_task_t *))
      );
#else
      __kmp_allocate( new_size * sizeof(kaapi_task_t *));
#endif
    for ( i = 0; i < queue->deque_T; ++i )
       new_deque[i+shift] = queue->deque[i];
#if LIBOMP_USE_AFFINITY
    numa_free((void*)queue->deque, size );
#else
    __kmp_free((void*)queue->deque);
#endif

    queue->deque_H += shift;
    queue->deque_T += shift;
    queue->deque = new_deque;
    queue->deque_size = new_size;
#endif
    return 0;
}


/*
*/
int kaapi_wsqueue_fini(kaapi_wsqueue_t* queue)
{
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_acquire_bootstrap_lock( &queue->deque_lock );
#endif
  KMP_DEBUG_ASSERT(queue->deque_H == 0);
  KMP_DEBUG_ASSERT(queue->deque_H == 0);

#if LIBOMP_USE_LINKED_DEQUEUE
#else // LIBOMP_USE_LINKED_DEQUEUE
#if LIBOMP_USE_AFFINITY
  numa_free((void*)queue->deque, queue->deque_size );
#else
  if (queue->deque) __kmp_free((void*)queue->deque);
#endif
  queue->deque = 0;
#endif
#if LIBOMP_USE_THE_AGGREGATION
  if (queue->tail)
  {
#if LIBOMP_USE_AFFINITY
    numa_free((void*)queue->tail, sizeof(kaapi_request_node_t) );
#else
    if (queue->tail) __kmp_free(queue->tail);
#endif
    queue->tail = 0;
  }
#else // !LIBOMP_USE_THE_AGGREGATION
  __kmp_release_bootstrap_lock( &queue->deque_lock );
#endif
  return 0;
}



#if LIBOMP_USE_THE_AGGREGATION
/** Per thread steal context
*/
__thread kaapi_request_node_t* _kaapi_sealcontext_key = 0;

/*
*/
kaapi_request_t* kaapi_sched_ccsync_post_request (
  kaapi_wsqueue_t* queue
)
{
  kaapi_request_node_t* currNode;
  kaapi_request_node_t* nextNode;

  nextNode = _kaapi_sealcontext_key;
  if (nextNode ==0)
    nextNode = (kaapi_request_node_t*)
#if LIBOMP_USE_AFFINITY
      numa_alloc_local( sizeof(kaapi_request_node_t));
#else
      __kmp_allocate( sizeof(kaapi_request_node_t));
#endif
  else
    _kaapi_sealcontext_key = nextNode->pgo_next;

  nextNode->next = 0;
  nextNode->wait = 1;
  nextNode->completed = 0;
  nextNode->pgo_next            = 0;
  nextNode->req.header.ident    = -1;
  nextNode->req.header.queue    = 0;
  nextNode->req.header.mask_arch= 0;
  nextNode->req.header.status   = KAAPI_REQUEST_S_INIT;

  /* on return currNode contains the old value of tail */
  currNode = nextNode;
  KAAPI_ATOMIC_EXCHANGE(currNode, queue->tail);
  currNode->nextNode = nextNode;
  /* do not forget to have this information set before stealing ! */
  currNode->req.header.mask_arch = 0;
  nextNode->req.header.queue     = queue;
  currNode->req.header.status    = KAAPI_REQUEST_S_POSTED;
  return &currNode->req;
}


/*
*/
#define KAAPI_MAXREQUEST_COUNT 48
int kaapi_sched_ccsync_commit_request( kaapi_wsqueue_t* queue, kaapi_request_t* req )
{
  kaapi_request_node_t* currNode = (kaapi_request_node_t*)req;
  kaapi_writemem_barrier();
  /* link with next waiting node */
  currNode->next = currNode->nextNode;

  /* (2) In case of aggregation, lock after thief has posted its request.
     lock and re-test if they are yet posted requests on victim or not 
     if during tentaive of locking, a reply occurs, then return with reply
  */
  while (currNode->wait == 1)
  {
#if NANOSLEEP
    struct timespec ts = { 0, 1 }; 
    nanosleep( &ts, 0 ); 
#endif
    kaapi_slowdown_cpu();
  }

  if (currNode->completed)
  {
    currNode->pgo_next = _kaapi_sealcontext_key;
    _kaapi_sealcontext_key = currNode;
    return currNode->req.header.status;
  }

  /* become combiner: iterates over all requests and process first push, then pop, then steal
  */
#if KAAPI_REORDER_LIST
  int i;
  kaapi_request_node_t* popList[KAAPI_MAXREQUEST_COUNT];
  kaapi_request_node_t* stealList[KAAPI_MAXREQUEST_COUNT];
  int count_pop   = 0;
  int count_steal = 0;
#endif
  kaapi_request_node_t* tmpNode = currNode;

  int count = KAAPI_MAXREQUEST_COUNT;
  while (1)
  {
    kaapi_request_node_t* tmpNodeNext = tmpNode->next;
    kaapi_request_t* req = &tmpNode->req;
    kaapi_request_op_t op = req->header.op;
    switch (op)
    {
      case KAAPI_REQUEST_OP_PUSH:
      {
        if (0 == __kaapi_wsqueue_push_task(queue, 0, req->push_a.task))
          req->header.status = KAAPI_REQUEST_S_OK;
        else
          req->header.status = KAAPI_REQUEST_S_NOK;

        tmpNode->completed = 1;
        kaapi_writemem_barrier();
        tmpNode->wait = 0;
      } break;

      case KAAPI_REQUEST_OP_PUSH_REMOTE:
      {
        if (0 == __kaapi_wsqueue_push_task(queue, 1, req->push_a.task))
          req->header.status = KAAPI_REQUEST_S_OK;
        else
          req->header.status = KAAPI_REQUEST_S_NOK;

        tmpNode->completed = 1;
        kaapi_writemem_barrier();
        tmpNode->wait = 0;
      } break;

#if LIBOMP_USE_LINKED_DEQUEUE
      case KAAPI_REQUEST_OP_PUSHLIST:
      {
        if (0 == __kaapi_wsqueue_push_tasklist(queue, req->push_l.list))
          req->header.status = KAAPI_REQUEST_S_OK;
        else
          req->header.status = KAAPI_REQUEST_S_NOK;

        tmpNode->completed = 1;
        kaapi_writemem_barrier();
        tmpNode->wait = 0;
      } break;
#endif

#if KAAPI_REORDER_LIST
#warning "Reorder"
      case KAAPI_REQUEST_OP_POP:
      {
        kaapi_assert_debug( count_pop < KAAPI_MAXREQUEST_COUNT );
        popList[count_pop] = tmpNode;
        ++count_pop;
      } break;

      case KAAPI_REQUEST_OP_STEAL:
      {
        kaapi_assert_debug( count_steal < KAAPI_MAXREQUEST_COUNT );
        stealList[count_steal] = tmpNode;
        ++count_steal;
      } break;
#else
#warning "No reorder"
      case KAAPI_REQUEST_OP_POP:
      {
        kaapi_task_t* task = __kaapi_wsqueue_pop_task( queue );
        if (task !=0)
        {
          req->header.status = KAAPI_REQUEST_S_OK;
          req->pop_a.task =  task;
        }
        else
          req->header.status = KAAPI_REQUEST_S_NOK;
        tmpNode->completed = 1;
        kaapi_writemem_barrier();
        tmpNode->wait = 0;
      } break;

      case KAAPI_REQUEST_OP_STEAL:
      {
        kaapi_task_t* task = __kaapi_wsqueue_steal_task( queue );
        if (task !=0)
        {
          req->header.status = KAAPI_REQUEST_S_OK;
          req->steal_a.task =  task;
        }
        else
          req->header.status = KAAPI_REQUEST_S_NOK;
        tmpNode->completed = 1;
        kaapi_writemem_barrier();
        tmpNode->wait = 0;

      } break;
#endif

      default:
        kaapi_assert(0);
    }
    
    tmpNode = tmpNodeNext;
    if (tmpNode->next ==0) break;
    if (--count ==0) 
      break;
  }
//if (KAAPI_MAXREQUEST_COUNT-count >1)
//  printf("Aggregation: %i\n", KAAPI_MAXREQUEST_COUNT-count);

#if KAAPI_REORDER_LIST
  for (i=0; i<count_pop; ++i)
  {
    kaapi_request_node_t* tmp = popList[i];
    kaapi_request_t* req = &tmp->req;
    kaapi_assert_debug( req->header.op == KAAPI_REQUEST_OP_POP);
    kaapi_task_t* task = __kaapi_wsqueue_pop_task(queue);
    if (task !=0)
    {
      req->header.status = KAAPI_REQUEST_S_OK;
      req->pop_a.task =  task;
    }
    else
      req->header.status = KAAPI_REQUEST_S_NOK;
    tmp->completed = 1;
    kaapi_writemem_barrier();
    tmp->wait = 0;
  }

  /* process steal not implace request before steal request */
  for (i=0; i<count_steal; ++i)
  {
    kaapi_request_node_t* tmp = stealList[i];
    kaapi_request_t* req = &tmp->req;
    kaapi_assert_debug( tmp->req.header.op == KAAPI_REQUEST_OP_STEAL);
    kaapi_task_t* task = __kaapi_wsqueue_steal_task(queue);
    if (task !=0)
    {
      req->header.status = KAAPI_REQUEST_S_OK;
      req->steal_a.task =  task;
    }
    else
      req->header.status = KAAPI_REQUEST_S_NOK;
    tmp->completed = 1;
    kaapi_writemem_barrier();
    tmp->wait = 0;
  }
#endif

  /* update the combiner thread */
  kaapi_writemem_barrier();
  tmpNode->wait = 0;

  /* reinsert currNode */
  int status = currNode->req.header.status;
  currNode->pgo_next = _kaapi_sealcontext_key;
  _kaapi_sealcontext_key = currNode;

  return status;
}


/* asynchronous versions
*/
int kaapi_sched_ccsync_commit_request_async(
  struct kaapi_place_group_operation_t* kpgo,
  kaapi_wsqueue_t* queue,
  kaapi_request_t* req
)
{
  /* link new node (currNode) here, after all request fields are setup: make them visible to the
     rest of the list
  */
  kaapi_request_node_t* currNode = (kaapi_request_node_t*)req;
  currNode->next = currNode->nextNode;

  /* test if completed: return */
  if (!currNode->wait && currNode->completed)
  {
    currNode->pgo_next = _kaapi_sealcontext_key;
    _kaapi_sealcontext_key = currNode;
    return currNode->req.header.status;
  }

  /* push back request in the kpgo's list */
  currNode->pgo_next = 0;
  if (kpgo->tail)
    kpgo->tail->pgo_next = currNode;
  else
    kpgo->head = currNode;
  kpgo->tail = currNode;

  return KAAPI_REQUEST_S_POSTED;
}


/*
*/
int kaapi_sched_ccsync_pgo_init( kaapi_place_group_operation_t* kpgo )
{
  kpgo->head = kpgo->tail = 0;
  return 0;
}

/*
*/
int kaapi_sched_ccsync_pgo_wait( kaapi_place_group_operation_t* kpgo )
{
  kaapi_request_node_t* req_first = kpgo->head;
  while (req_first !=0)
  {
    kaapi_request_node_t* next = req_first->pgo_next;
    kaapi_sched_ccsync_commit_request( req_first->req.header.queue, &req_first->req );
    req_first = next;
  }
  kpgo->tail = kpgo->head = 0;
  return 0;
}

/*
*/
int kaapi_sched_ccsync_pgo_fini( kaapi_place_group_operation_t* kpgo )
{
  kaapi_assert_debug( kpgo->head ==  0);
  kaapi_assert_debug( kpgo->tail ==  0);
  return 0;
}
#endif // #if LIBOMP_USE_THE_AGGREGATION


/* Assume 1 owner and multiple thieves
*/
int __kaapi_wsqueue_push_task(kaapi_wsqueue_t* queue, int remote, kaapi_task_t* task )
{
  int err;
  KMP_DEBUG_ASSERT( task != 0);
#if LIBOMP_USE_LINKED_DEQUEUE
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_acquire_bootstrap_lock( & queue->deque_lock );
#endif
  /* push to head */
  task->prev = 0;
  task->next = queue->deque_H;
  if (task->next ==0)
    queue->deque_T = task;
  else
    queue->deque_H->prev = task;
  queue->deque_H = task;
#if 0
  /* push to tail */
  task->next = 0
  task->prev = queue->deque_T;
  if (task->prev ==0)
    queue->deque_H = task;
  else
    queue->deque_T->next = task;
  queue->deque_T = task;
#endif
  ++queue->deque_size;
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_release_bootstrap_lock( & queue->deque_lock );
#endif
  return 0;
#else // LIBOMP_USE_LINKED_DEQUEUE
  if (!remote)
  {
reread:
    uint32_t top = queue->deque_T;
    //KMP_MB(); /* not really implemented ? see kmp_os.h */
    queue->deque[top] = task;
    if (top+1 >= queue->deque_size)
    {
#if !LIBOMP_USE_THE_AGGREGATION
      __kmp_acquire_bootstrap_lock( & queue->deque_lock );
#endif
      if (queue->deque_H >= queue->deque_T)
      {
        queue->deque_H = queue->deque_size/2;
        queue->deque_T = queue->deque_size/2;
        top = queue->deque_T;
        queue->deque[top] = task;
      }
      if (top+1 >= queue->deque_size)
        kaapi_wsqueue_realloc(queue);
#if !LIBOMP_USE_THE_AGGREGATION
      __kmp_release_bootstrap_lock( & queue->deque_lock );
#endif
      goto reread;
    }
    __sync_synchronize();
    queue->deque_T = top +1;
    err = 0;
  }
  else
  { 
    /* push as if it was a remote push : from head of the queue until the it was not empty */
#if !LIBOMP_USE_THE_AGGREGATION
    __kmp_acquire_bootstrap_lock( & queue->deque_lock );
#endif
redo_read:
    uint32_t head = queue->deque_H;
    if (head >0)
    {
      queue->deque[head-1] = task;
      __sync_synchronize();
      queue->deque_H = head-1;
      err = 0;
    }
    else
    {
      kaapi_wsqueue_realloc_push_remote( queue );
      err = ENOMEM;
      goto redo_read;
    }
#if !LIBOMP_USE_THE_AGGREGATION
    __kmp_release_bootstrap_lock( & queue->deque_lock );
#endif
  }
#endif // LIBOMP_USE_LINKED_DEQUEUE
  return err;
}


#if LIBOMP_USE_LINKED_DEQUEUE
int __kaapi_wsqueue_push_tasklist(kaapi_wsqueue_t* queue, kaapi_wsqueue_t* list )
{
  KMP_DEBUG_ASSERT( list != 0);
  if (kaapi_wsqueue_empty( list )) return 0;
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_acquire_bootstrap_lock( & queue->deque_lock );
#endif
  /* push to tail */
  list->deque_T->next = 0;
  list->deque_H->prev = queue->deque_T;
  if (list->deque_H->prev ==0)
    queue->deque_H = list->deque_H;
  else
    queue->deque_T->next = list->deque_H;
  queue->deque_T = list->deque_T;
  queue->deque_size += list->deque_size;;

  list->deque_H = list->deque_T = 0;
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_release_bootstrap_lock( & queue->deque_lock );
#endif
  return 0;
}
#endif // LIBOMP_USE_LINKED_DEQUEUE

/* Assume N owners and multiple thieves
*/
static inline int __kaapi_wsqueue_locked_push_task(kaapi_wsqueue_t* queue, int remote, kaapi_task_t* task )
{
  int retval;
#if LIBOMP_USE_LINKED_DEQUEUE // always locked
  retval = __kaapi_wsqueue_push_task( queue, remote, task );
#else
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_acquire_bootstrap_lock( &queue->deque_lock_owner );
#endif
  retval = __kaapi_wsqueue_push_task( queue, remote, task );
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_release_bootstrap_lock( &queue->deque_lock_owner );
#endif
#endif
  return retval;
}



/*
*/
kaapi_task_t* __kaapi_wsqueue_pop_task( kaapi_wsqueue_t* queue )
{
  kaapi_task_t* task =0;

#if LIBOMP_USE_LINKED_DEQUEUE
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_acquire_bootstrap_lock( & queue->deque_lock );
#endif
  /* pop from Head */
  if (queue->deque_H)
  {
    task = queue->deque_H;
    queue->deque_H = task->next;
    if (queue->deque_H ==0)
      queue->deque_T = 0;
    else
      task->next->prev = 0;

    task->prev = task->next = 0;
  }
  --queue->deque_size;
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_release_bootstrap_lock( &queue->deque_lock );
#endif
  return task;
#else // LIBOMP_USE_LINKED_DEQUEUE

  uint32_t deque_tail = queue->deque_T;
  if (queue->deque_H >= deque_tail)
    return 0;

  uint32_t new_tail = deque_tail - 1;
  queue->deque_T = new_tail;
  __sync_synchronize();
  //KMP_MB(); /* not implemented ? see kmp_os.h */
  if (queue->deque_H > new_tail)
  { /* conflict pop-steal, rollback then lock */
    queue->deque_T = deque_tail;
#if !LIBOMP_USE_THE_AGGREGATION
    __kmp_acquire_bootstrap_lock( &queue->deque_lock );
#endif
    deque_tail = queue->deque_T;
    new_tail = deque_tail - 1;
    queue->deque_T = new_tail;
    if (queue->deque_H > queue->deque_T) /* empty ? => rollback */
    {
      queue->deque_H = queue->deque_size/2;
      queue->deque_T = queue->deque_size/2;
      task = 0;
    }
    else
    {
      task = queue->deque[ new_tail ];
      KMP_DEBUG_ASSERT( task != 0);
    }
#if !LIBOMP_USE_THE_AGGREGATION
    __kmp_release_bootstrap_lock( &queue->deque_lock );
#endif
    return task;
  }
  return queue->deque[ new_tail ];
#endif
}


/*
*/
static inline kaapi_task_t* __kaapi_wsqueue_locked_pop_task( kaapi_wsqueue_t* queue )
{
  kaapi_task_t* task;
#if LIBOMP_USE_LINKED_DEQUEUE
  task = __kaapi_wsqueue_pop_task( queue );
#else
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_acquire_bootstrap_lock( &queue->deque_lock_owner );
#endif
  task = __kaapi_wsqueue_pop_task( queue );
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_release_bootstrap_lock( &queue->deque_lock_owner );
#endif
#endif
  return task;
}


/*
*/
kaapi_task_t* __kaapi_wsqueue_steal_task( kaapi_wsqueue_t* queue )
{
  kaapi_task_t* task =0;
#if LIBOMP_USE_LINKED_DEQUEUE
  return __kaapi_wsqueue_pop_task(queue);
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_acquire_bootstrap_lock( & queue->deque_lock );
#endif
  /* steal from tail */
  if (queue->deque_T)
  {
    task = queue->deque_T;
    queue->deque_T = task->prev;
    if (queue->deque_T ==0)
      queue->deque_H = 0;
    else
      task->prev->next = 0;

    task->prev = task->next = 0;
    --queue->deque_size;
  }
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_release_bootstrap_lock( & queue->deque_lock );
#endif
  return task;

#else // LIBOMP_USE_LINKED_DEQUEUE

  uint32_t deque_head;
  uint32_t deque_tail;

  /* Thief lock */
#if !LIBOMP_USE_THE_AGGREGATION
  __kmp_acquire_bootstrap_lock( & queue->deque_lock );
#endif
  deque_head = queue->deque_H;
  uint32_t new_head = deque_head + 1;
  queue->deque_H = new_head;
  __sync_synchronize();
  //KMP_MB(); /* not implemented ? see kmp_os.h */
  if (new_head > queue->deque_T)
  { /* conflict steal-pop, rollback then failure */
    queue->deque_H = deque_head;
    task = 0;
  }
  else
    task = queue->deque[ deque_head ];
#if !LIBOMP_USE_THE_AGGREGATION
  /* Thief unlock */
  __kmp_release_bootstrap_lock( & queue->deque_lock );
#endif
#endif // LIBOMP_USE_LINKED_DEQUEUE
  return task;
}



/*  Client
*/
int kaapi_wsqueue_push_task(kaapi_wsqueue_t* queue, int remote, kaapi_task_t* task )
{
#if LIBOMP_USE_THE_AGGREGATION
  kaapi_request_t* request = kaapi_sched_ccsync_post_request(queue);
  request->header.ident = 0; /* queue->kid; */
  if (remote)
    request->header.op  = KAAPI_REQUEST_OP_PUSH_REMOTE;
  else
    request->header.op  = KAAPI_REQUEST_OP_PUSH;
  request->push_a.task  = task;

  kaapi_sched_ccsync_commit_request(queue, request);

  return kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK ? 0 : EINVAL;
#else
  return __kaapi_wsqueue_push_task( queue, remote, task );
#endif
}

#if LIBOMP_USE_LINKED_DEQUEUE
extern int kaapi_wsqueue_push_tasklist(kaapi_wsqueue_t* queue, kaapi_wsqueue_t* list )
{
#if LIBOMP_USE_THE_AGGREGATION
  kaapi_request_t* request = kaapi_sched_ccsync_post_request(queue);
  request->header.ident = 0; /* queue->kid; */
  request->header.op  = KAAPI_REQUEST_OP_PUSHLIST;
  request->push_l.list  = list;

  kaapi_sched_ccsync_commit_request(queue, request);

  return kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK ? 0 : EINVAL;
#else
  return __kaapi_wsqueue_push_tasklist( queue, list );
#endif
}
#endif

/*  Client
*/
int kaapi_wsqueue_locked_push_task(kaapi_wsqueue_t* queue, int remote, kaapi_task_t* task )
{
#if LIBOMP_USE_THE_AGGREGATION
  kaapi_request_t* request = kaapi_sched_ccsync_post_request(queue);
  request->header.ident = 0; /* queue->kid; */
  if (remote)
    request->header.op  = KAAPI_REQUEST_OP_PUSH_REMOTE;
  else
    request->header.op  = KAAPI_REQUEST_OP_PUSH;
  request->push_a.task  = task;

  kaapi_sched_ccsync_commit_request(queue, request);

  return kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK ? 0 : EINVAL;
#else
  return __kaapi_wsqueue_locked_push_task( queue, remote, task );
#endif
}


/*  Client
*/
kaapi_task_t* kaapi_wsqueue_pop_task( kaapi_wsqueue_t* queue )
{
#if LIBOMP_USE_THE_AGGREGATION
  kaapi_request_t* request = kaapi_sched_ccsync_post_request(queue);
  request->header.ident = 0; /* queue->kid; */
  request->header.op    = KAAPI_REQUEST_OP_POP;

  kaapi_sched_ccsync_commit_request(queue, request);

  return (kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK ?
              request->pop_a.task
            : 0);
#else
  return __kaapi_wsqueue_pop_task(queue);
#endif
}


/*  Client
*/
kaapi_task_t* kaapi_wsqueue_locked_pop_task( kaapi_wsqueue_t* queue )
{
#if LIBOMP_USE_THE_AGGREGATION
  kaapi_request_t* request = kaapi_sched_ccsync_post_request(queue);
  request->header.ident = 0; /* queue->kid; */
  request->header.op    = KAAPI_REQUEST_OP_POP;

  kaapi_sched_ccsync_commit_request(queue, request);

  return (kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK ?
              request->pop_a.task
            : 0);
#else
  return __kaapi_wsqueue_locked_pop_task(queue);
#endif
}


/*  Client
*/
kaapi_task_t* kaapi_wsqueue_steal_task( kaapi_wsqueue_t* queue )
{
#if LIBOMP_USE_THE_AGGREGATION
  kaapi_request_t* request = kaapi_sched_ccsync_post_request(queue);
  request->header.ident = 0; /* queue->kid; */
  request->header.op    = KAAPI_REQUEST_OP_STEAL;

  kaapi_sched_ccsync_commit_request(queue, request);

  return (kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK ?
              request->steal_a.task
            : 0);
#else
  return __kaapi_wsqueue_steal_task(queue);
#endif
}


// Insert the sublist [itBegin .. itEnd] after itInsert in queue
void kaapi_wsqueue_splice(
      kaapi_wsqueue_t* queue,
      kaapi_task_t* itInsert,
      kaapi_task_t* itBegin,
      kaapi_task_t* itEnd )
{
#if LIBOMP_USE_LINKED_DEQUEUE
  kaapi_task_t* nextItinsert = itInsert->next;
  kaapi_task_t* prevItBegin = itBegin->prev;
  kaapi_task_t* nextItEnd = itEnd->next;

#ifndef NDEBUG
  // Note: itBegin == itEnd is supported
  KMP_DEBUG_ASSERT(itInsert != itBegin); // Not supported yet
  KMP_DEBUG_ASSERT(itInsert != itEnd); // Not supported yet
  for(kaapi_task_t* it=itBegin ; it!=itEnd ; it=it->next)
  {
    KMP_DEBUG_ASSERT(it != NULL); // Corruption: NULL reached before itEnd
    KMP_DEBUG_ASSERT(it != itInsert); // Corruption: insert detected
  }
#endif

  // slice removal
  if(prevItBegin != 0)
    prevItBegin->next = nextItEnd;
  else
    queue->deque_H = nextItEnd;
  if(nextItEnd != 0)
    nextItEnd->prev = prevItBegin;
  else
    queue->deque_T = prevItBegin;

  // slice insertion
  itInsert->next = itBegin;
  itBegin->prev = itInsert;
  if(nextItinsert != 0)
    nextItinsert->prev = itEnd;
  else
    queue->deque_T = itEnd;

  itEnd->next = nextItinsert;
#endif
}
