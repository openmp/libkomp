/*
 * kmp_taskdeps.cpp
 */


//===----------------------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.txt for details.
//
//===----------------------------------------------------------------------===//


//#define KMP_SUPPORT_GRAPH_OUTPUT 1

#include "kmp.h"
#include "kmp_io.h"
#include "kmp_wait_release.h"


#if LIBOMP_USE_PARALLEL_SPAWN
#include "kmp_atomic.h"
#endif

#if OMP_40_ENABLED && LIBOMP_USE_REORDER4LOCALITY
#include "kmp_taskreschedule.h"
#endif


#if OMP_40_ENABLED

// TODO: Improve memory allocation? keep a list of pre-allocated structures?
// allocate in blocks? re-use list finished list entries?
// TODO: don't use atomic ref counters for stack-allocated nodes.
// TODO: find an alternate to atomic refs for heap-allocated nodes?
// TODO: Finish graph output support
// TODO: kmp_lock_t seems a tad to big (and heavy weight) for this. Check other
// runtime locks
// TODO: Any ITT support needed?

#ifdef KMP_SUPPORT_GRAPH_OUTPUT
static kmp_int32 kmp_node_id_seed = 0;
#endif

static void __kmp_init_node(kmp_depnode_t *node) {
  node->dn.task = NULL; // set to null initially, it will point to the right
  // task once dependences have been processed
  node->dn.successors = NULL;
  __kmp_init_lock(&node->dn.lock);
  node->dn.nrefs = 1; // init creates the first reference to the node
#ifdef KMP_SUPPORT_GRAPH_OUTPUT
  node->dn.id = KMP_TEST_THEN_INC32(&kmp_node_id_seed);
#endif
#if LIBOMP_USE_CONCURRENT_WRITE
    node->dn.cw = 0;
#endif
}

static inline kmp_depnode_t *__kmp_node_ref(kmp_depnode_t *node) {
  KMP_TEST_THEN_INC32(&node->dn.nrefs);
  return node;
}

static inline void __kmp_node_deref(kmp_info_t *thread, kmp_depnode_t *node) {
  if (!node)
    return;

  kmp_int32 n = KMP_TEST_THEN_DEC32(&node->dn.nrefs) - 1;
  if (n == 0) {
    KMP_ASSERT(node->dn.nrefs == 0);
#if USE_FAST_MEMORY
    __kmp_fast_free(thread, node);
#else
    __kmp_thread_free(thread, node);
#endif
  }
}

#define KMP_ACQUIRE_DEPNODE(gtid, n) __kmp_acquire_lock(&(n)->dn.lock, (gtid))
#define KMP_RELEASE_DEPNODE(gtid, n) __kmp_release_lock(&(n)->dn.lock, (gtid))

static void __kmp_depnode_list_free(kmp_info_t *thread, kmp_depnode_list *list);

#if (LIBOMP_USE_DYNHASH==0)
enum { KMP_DEPHASH_OTHER_SIZE = 97, KMP_DEPHASH_MASTER_SIZE = 997 };
#define KMP_DEPHASH_SIZE(h_size) (h_size)
#else
enum { KMP_DEPHASH_OTHER_SIZE_BITS = 10 };
#define KMP_DEPHASH_SIZE(h_size) (1 << (h_size))
#endif

static inline kmp_int32 __kmp_dephash_hash(kmp_intptr_t addr, size_t hsize) {
#if LIBOMP_USE_DYNHASH // TG: orignal hash function seems to generate a lot of conflict !
#if 0
  return ((addr >> 6) ^ (addr >> 2)) % KMP_DEPHASH_SIZE(hsize);
#else
  kmp_intptr_t v = 0;
  v ^= (addr >> 32);
  v ^= (addr >> 16);
  v ^= (addr >> 8); /* 8 in Kaapi */
  v ^= (addr >> 2); /* better that 8 */
  return ((uint32_t) v) & (KMP_DEPHASH_SIZE(hsize)-1);
#endif
#else
  // TODO alternate to try: set = (((Addr64)(addrUsefulBits * 9.618)) %
  // m_num_sets );
  return ((addr >> 6) ^ (addr >> 2)) % hsize;
#endif
}

static kmp_dephash_t *__kmp_dephash_create(kmp_info_t *thread,
                                           kmp_taskdata_t *current_task) {
  kmp_dephash_t *h;

  size_t h_size;

#if (LIBOMP_USE_DYNHASH==0)
  if (current_task->td_flags.tasktype == TASK_IMPLICIT)
    h_size = KMP_DEPHASH_MASTER_SIZE;
  else
    h_size = KMP_DEPHASH_OTHER_SIZE;
#else
  h_size = KMP_DEPHASH_OTHER_SIZE_BITS;
#endif

  size_t size = KMP_DEPHASH_SIZE(h_size) * sizeof(kmp_dephash_bucket_t);

#if USE_FAST_MEMORY
  h = (kmp_dephash_t *)__kmp_fast_allocate(thread, sizeof(kmp_dephash_t));
  h->buckets = (kmp_dephash_bucket_t*)__kmp_fast_allocate(thread, size);
#else
  h = (kmp_dephash_t *)__kmp_thread_malloc(thread, sizeof(kmp_dephash_t));
  h->buckets = (kmp_dephash_bucket_t*)__kmp_thread_malloc(thread, size);
#endif
  h->size = h_size;

#ifdef KMP_DEBUG
  h->nelements = 0;
  h->nconflicts = 0;
#endif

  for (size_t i = 0; i < KMP_DEPHASH_SIZE(h_size); i++)
  {
    h->buckets[i].e = 0;
#if LIBOMP_USE_PARALLEL_SPAWN
    /* inspired from TSO lock in kaapi_atomic.h */
    KAAPI_ATOMIC_WRITE(&h->buckets[i].w_lock,1);
#endif
  }

  return h;
}

#if LIBOMP_USE_DYNHASH
static void __kmp_dephash_resize(kmp_info_t *thread,
                                 kmp_dephash_t *h)
{
  kmp_dephash_bucket_t* old_buckets;
  kmp_dephash_bucket_t* new_buckets;

  size_t old_size = h->size;
#if (LIBOMP_USE_DYNHASH==0)
  size_t h_size = old_size*4;
#else
  size_t h_size = old_size+2; /* #bits for the size */
#endif

  size_t size = KMP_DEPHASH_SIZE(h_size) * sizeof(kmp_dephash_bucket_t);

#if USE_FAST_MEMORY
  new_buckets = (kmp_dephash_bucket_t*)__kmp_fast_allocate(thread, size);
#else
  new_buckets = (kmp_dephash_bucket_t*)__kmp_thread_malloc(thread, size);
#endif

#if defined(KMP_DEBUG) || LIBOMP_USE_DYNHASH
  h->nelements = 0;
  h->nconflicts = 0;
#endif
  old_buckets = h->buckets;
  for (size_t i = 0; i < KMP_DEPHASH_SIZE(h_size); i++)
  {
    new_buckets[i].e = 0;
#if LIBOMP_USE_PARALLEL_SPAWN
    /* inspired from TSO lock in kaapi_atomic.h */
    KAAPI_ATOMIC_WRITE(&new_buckets[i].w_lock,1);
#endif
  }

  for (size_t i = 0; i < KMP_DEPHASH_SIZE(old_size); i++)
  {
    kmp_dephash_entry_t *entry;
    kmp_dephash_entry_t *next_entry;
    /* lock buckets[i] */
    for (entry = old_buckets[i].e; entry; entry = next_entry)
    {
      /* rehash */
      kmp_int32 bucket = __kmp_dephash_hash(entry->addr, h_size);
      next_entry = entry->next_in_bucket; 
      entry->next_in_bucket = new_buckets[bucket].e;
      new_buckets[bucket].e = entry;
#if defined(KMP_DEBUG) || LIBOMP_USE_DYNHASH 
      h->nelements++;
      if (entry->next_in_bucket)
        h->nconflicts++;
#endif
    }
    old_buckets[i].e = 0;
    /* unlock buckets[i] */
  }

  h->size = h_size;
  h->buckets = new_buckets;

#if USE_FAST_MEMORY
  __kmp_fast_free(thread, old_buckets);
#else
  __kmp_thread_free(thread, old_buckets);
#endif
}
#endif


void __kmp_dephash_free_entries(kmp_info_t *thread, kmp_dephash_t *h) {
  for (size_t i = 0; i < KMP_DEPHASH_SIZE(h->size); i++) {
#if LIBOMP_USE_PARALLEL_SPAWN
acquire:
    if (KAAPI_ATOMIC_DECR(&h->buckets[i].w_lock) !=0)
    { /* another writer somewhere */
      while (KAAPI_ATOMIC_READ(&h->buckets[i].w_lock) <=0)
        kaapi_slowdown_cpu();
      goto acquire;
    }
#endif
    if (h->buckets[i].e) {
      kmp_dephash_entry_t *next;
      for (kmp_dephash_entry_t *entry = h->buckets[i].e; entry; entry = next) {
        next = entry->next_in_bucket;
        if (entry->last_ins) __kmp_depnode_list_free(thread, entry->last_ins);
        if (entry->last_out) __kmp_depnode_list_free(thread, entry->last_out);
#if USE_FAST_MEMORY
        __kmp_fast_free(thread, entry);
#else
        __kmp_thread_free(thread, entry);
#endif
      }
      h->buckets[i].e = 0;
    }
#if LIBOMP_USE_PARALLEL_SPAWN
    KAAPI_ATOMIC_WRITE(&h->buckets[i].w_lock,1);
#endif
  }
#if defined(KMP_DEBUG) || LIBOMP_USE_DYNHASH
  h->nelements  = 0;
  h->nconflicts = 0;
#endif
}

void __kmp_dephash_free(kmp_info_t *thread, kmp_dephash_t *h) {
  __kmp_dephash_free_entries(thread, h);
#if USE_FAST_MEMORY
  __kmp_fast_free(thread, h);
#else
  __kmp_thread_free(thread, h);
#endif
}

static kmp_dephash_entry *
__kmp_dephash_find(kmp_info_t *thread, kmp_dephash_t *h, kmp_intptr_t addr) {

  kmp_int32 bucket = __kmp_dephash_hash(addr, h->size);

  kmp_dephash_entry_t *entry;
redo_:
  for (entry = h->buckets[bucket].e; entry; entry = entry->next_in_bucket)
    if (entry->addr == addr)
      break;

  if (entry == NULL) {
#if LIBOMP_USE_PARALLEL_SPAWN // If I'm alone do it else abort and refind
redo:
    if (KAAPI_ATOMIC_DECR( &h->buckets[bucket].w_lock ) !=0)
    { /* another writer somewhere */
      while (KAAPI_ATOMIC_READ(&h->buckets[bucket].w_lock) <=0)
        kaapi_slowdown_cpu();
      goto redo;
    }
    for (entry = h->buckets[bucket].e; entry; entry = entry->next_in_bucket)
      if (entry->addr == addr)
      {
        KAAPI_ATOMIC_WRITE(&h->buckets[bucket].w_lock, 1);
        return entry;
      }
#endif
// create entry. This is only done by one thread so no locking required
#if USE_FAST_MEMORY
    entry = (kmp_dephash_entry_t *)__kmp_fast_allocate(
        thread, sizeof(kmp_dephash_entry_t));
#else
    entry = (kmp_dephash_entry_t *)__kmp_thread_malloc(
        thread, sizeof(kmp_dephash_entry_t));
#endif
    entry->addr = addr;
    entry->last_out = NULL;
    entry->last_ins = NULL;
#if LIBOMP_USE_CONCURRENT_WRITE
    entry->cw_sync = NULL;
#endif
    entry->next_in_bucket = h->buckets[bucket].e;
    h->buckets[bucket].e = entry;
#if defined(KMP_DEBUG) || LIBOMP_USE_DYNHASH 
    h->nelements++;
    if (entry->next_in_bucket)
      h->nconflicts++;
#endif
#if LIBOMP_USE_PARALLEL_SPAWN
    KAAPI_ATOMIC_WRITE( &h->buckets[bucket].w_lock, 1 );
#endif
  }
  return entry;
}

static kmp_depnode_list_t *__kmp_add_node(kmp_info_t *thread,
                                          kmp_depnode_list_t *list,
                                          kmp_depnode_t *node) {
  kmp_depnode_list_t *new_head;

#if USE_FAST_MEMORY
  new_head = (kmp_depnode_list_t *)__kmp_fast_allocate(
      thread, sizeof(kmp_depnode_list_t));
#else
  new_head = (kmp_depnode_list_t *)__kmp_thread_malloc(
      thread, sizeof(kmp_depnode_list_t));
#endif

  new_head->node = __kmp_node_ref(node);
  new_head->next = list;

  return new_head;
}

static void __kmp_depnode_list_free(kmp_info_t *thread,
                                    kmp_depnode_list *list) {
  kmp_depnode_list *next;

  for (; list; list = next) {
    next = list->next;

    __kmp_node_deref(thread, list->node);
#if USE_FAST_MEMORY
    __kmp_fast_free(thread, list);
#else
    __kmp_thread_free(thread, list);
#endif
  }
}

static inline void __kmp_track_dependence(kmp_int32 gtid,
                                          kmp_depnode_t *source,
                                          kmp_depnode_t *sink,
                                          kmp_task_t *sink_task) {
#ifdef KMP_SUPPORT_GRAPH_OUTPUT
  kmp_taskdata_t *task_source = KMP_TASK_TO_TASKDATA(source->dn.task);
  // do not use sink->dn.task as that is only filled after the dependencies
  // are already processed!
  kmp_taskdata_t *task_sink = KMP_TASK_TO_TASKDATA(sink_task);

  __kmp_printf("%d(%s) -> %d(%s)\n", source->dn.id,
               task_source->td_ident->psource, sink->dn.id,
               task_sink->td_ident->psource);
#endif
#if OMPT_SUPPORT && OMPT_TRACE
  // OMPT tracks dependences between task (a=source, b=sink) in which
  // task a blocks the execution of b through the ompt_new_dependence_callback
  if (ompt_enabled &&
      ompt_callbacks.ompt_callback(ompt_event_task_dependence_pair)) {
    kmp_taskdata_t *task_source = KMP_TASK_TO_TASKDATA(source->dn.task);
    kmp_taskdata_t *task_sink = KMP_TASK_TO_TASKDATA(sink_task);

    ompt_callbacks.ompt_callback(ompt_event_task_dependence_pair)(
        GTID_TO_OMPT_THREAD_ID(gtid),
        task_source->ompt_task_info.task_id,
        task_sink->ompt_task_info.task_id);
  }
#endif /* OMPT_SUPPORT && OMPT_TRACE */
}



#include <stdio.h>


#if LIBOMP_USE_CONCURRENT_WRITE
static kmp_task_t dummy_cw_node;

/* Lock is acquired
*/
static inline void
__kmp_enqueue ( kmp_int32 gtid, kmp_depnode_t *node, kmp_cw_depnode_t *cw )
{
  kmp_info_t *thread = __kmp_threads[ gtid ];
  kmp_depnode_list_t *dnode;
#if USE_FAST_MEMORY
  dnode = (kmp_depnode_list_t *) __kmp_fast_allocate(thread,sizeof(kmp_depnode_list_t));
#else
  dnode = (kmp_depnode_list_t *) __kmp_thread_malloc(thread,sizeof(kmp_depnode_list_t));
#endif
  /* fifo */
  dnode->node = __kmp_node_ref(node);
  dnode->next = 0;
  if (cw->tail_waiter ==0)
    cw->head_waiter = dnode;
  else
    cw->tail_waiter->next = dnode;
  cw->tail_waiter = dnode;
}

/* acquired all commute accesses or fail
   return 0 if success to acquired commute accesses
   return 1 if failure
*/
static int __kmp_acquired_or_failed( kmp_int32 gtid,
                                     kmp_depnode_t *node,
                                     kmp_cw_depnode_t* c_sync )
{
//  if( list_commute ==0) return 0;
  if (c_sync ==0) return 0;
  if (!c_sync->flags.commute) return 0;
  /* lock n-commute arguments, it not possible release all of them */
  int fail = 0;
//  TG: todo implement algorithm for multiple commute and ressource reservation
//  kmp_depnode_list_t* curr;
//  for ( curr = list_commute; curr; curr = curr->next )
  {
    /* commute: make task ready only if the shared ressource is not alread locked
       tasks are (== graphe nodes) enqueued in info->last_waiter and npred is incremented.
       on release all cw will decrease it.
    */
    //kmp_cw_depnode_t* c_sync = (kmp_cw_depnode_t*)curr->node;
    KMP_ACQUIRE_DEPNODE(gtid, &c_sync->in_node);
    if (c_sync->state ==1)
    {
      fail = 1;
      __kmp_enqueue( gtid, node, c_sync );
      KMP_RELEASE_DEPNODE(gtid, &c_sync->in_node);
      //break;
      //printf("Task: %p (%p) not ready\n", node->dn.task, node );
      return fail;
    }
    c_sync->state = 1;
    KMP_RELEASE_DEPNODE(gtid, &c_sync->in_node);
  }
  //printf("Task: %p (%p) ready\n", node->dn.task, node );
  return fail;
}
#endif //LIBOMP_USE_CONCURRENT_WRITE


#if !LIBOMP_USE_CONCURRENT_WRITE
# define IS_WRITE(x) (x.out)
#else
# define IS_WRITE(x) (x.out || x.cw || x.commute)
# define IS_CONCURRENTWRITE(x) (x.cw || x.commute)
static int inline IS_CONCURRENT(kmp_depend_info_flags_t a, kmp_depend_info_flags_t b)
{
  int retval =  ((a.in && !a.out && b.in && !b.out) || (a.cw && b.cw) || (a.commute && b.commute));
  return retval;
}
#endif

/* To handle CW:
   - first CW -> create dummy node used to detect end of concurrent accesses
   - new r,rw are branched afterward this dummy node
   Store dummy node in info->
   Dummy nodes may have task set to 0, then __kmp_release_deps may also release_deps
   the successors this dummy node.
   Todo: best algorithm to avoid O(k) iteration of previous accesses.
*/
template <bool filter>
static inline kmp_int32
__kmp_process_deps(kmp_int32 gtid, kmp_depnode_t *node, kmp_dephash_t *hash,
                   bool dep_barrier, kmp_int32 ndeps,
                   kmp_depend_info_t *dep_list, kmp_task_t *task) {
  KA_TRACE(30, ("__kmp_process_deps<%d>: T#%d processing %d dependencies : "
                "dep_barrier = %d\n",
                filter, gtid, ndeps, dep_barrier));

  kmp_info_t *thread = __kmp_threads[gtid];
  kmp_int32 npredecessors = 0;
  for (kmp_int32 i = 0; i < ndeps; i++) {
    const kmp_depend_info_t *dep = &dep_list[i];

    KMP_DEBUG_ASSERT(dep->flags.in);

    if (filter &&
#if LIBOMP_USE_VARDEP && OMP_40_ENABLED
        dep->flags.alias == 1
#else
        dep->base_addr == 0
#endif
    )
      continue; // skip filtered entries

    kmp_dephash_entry_t *info =
        __kmp_dephash_find(thread, hash, dep->base_addr);
#if LIBOMP_USE_CONCURRENT_WRITE
    if (IS_CONCURRENTWRITE(dep->flags))
    {
      if (info->cw_sync ==0)
      {
#if USE_FAST_MEMORY
        info->cw_sync = (kmp_cw_depnode_t *) __kmp_fast_allocate(thread,sizeof(kmp_cw_depnode_t));
#else
        info->cw_sync = (kmp_cw_depnode_t *) __kmp_thread_malloc(thread,sizeof(kmp_cw_depnode_t));
#endif
        __kmp_init_node( &info->cw_sync->in_node );
        info->cw_sync->in_node.dn.npredecessors = -1;
        info->cw_sync->in_node.dn.task = &dummy_cw_node;
        info->cw_sync->head_waiter = info->cw_sync->tail_waiter = 0;
        info->cw_sync->nwriters = 0;
        info->cw_sync->state = 0;
        info->cw_sync->flags = dep->flags;
        info->flags = {0, 0, 0, 0};
      }

      /* TODO: detect if the same cw is set multiple times in the task */
      KMP_ASSERT(node->dn.cw ==0);
      node->dn.cw = info->cw_sync;
      ++info->cw_sync->nwriters;
    }
#endif /* #if LIBOMP_USE_CONCURRENT_WRITE */

    if (IS_WRITE(dep->flags) && info->last_ins) {
      kmp_depnode_t* outdep;
#if LIBOMP_USE_CONCURRENT_WRITE
      if (IS_CONCURRENTWRITE(dep->flags))
        outdep = &info->cw_sync->in_node;
      else
#endif
        outdep = node;
#if LIBOMP_USE_CONCURRENT_WRITE
      int npred_cw = 1; /* for -1 initial value */
#endif
      for (kmp_depnode_list_t * p = info->last_ins; p; p = p->next) {
        kmp_depnode_t * indep = p->node;
        if ( indep->dn.task ) {
          KMP_ACQUIRE_DEPNODE(gtid,indep);
          if ( indep->dn.task ) {
            __kmp_track_dependence(gtid, indep, outdep, task);
            indep->dn.successors =
                __kmp_add_node(thread, indep->dn.successors, outdep);
#if LIBOMP_USE_REORDER4LOCALITY
            kaapi_reorder4locality_addDependency(
              thread->th.th_tasklist,
              KMP_TASK_TO_TASKDATA(indep->dn.task),
              KMP_TASK_TO_TASKDATA(task),
              dep
            );
#endif
            KA_TRACE(40, ("__kmp_process_deps<%d>: T#%d adding dependence from "
                          "%p to %p\n",
                          filter, gtid, KMP_TASK_TO_TASKDATA(indep->dn.task),
                          KMP_TASK_TO_TASKDATA(task)));
#if LIBOMP_USE_CONCURRENT_WRITE
            if (IS_CONCURRENTWRITE(dep->flags))
              ++npred_cw;
            else
#endif
              ++npredecessors;
          }
          KMP_RELEASE_DEPNODE(gtid,indep);
        }
      }
#if LIBOMP_USE_CONCURRENT_WRITE
      if (IS_CONCURRENTWRITE(dep->flags) && (npred_cw>1))
      {
        KMP_ACQUIRE_DEPNODE(gtid,outdep);
        if (KMP_TEST_THEN_ADD32(&outdep->dn.npredecessors, npred_cw) >0)
        {
          outdep->dn.successors =
              __kmp_add_node(thread, outdep->dn.successors, node);
          ++npredecessors;
        }
        else {
          outdep->dn.task = 0;
        }
        KMP_RELEASE_DEPNODE(gtid,outdep);
      }
#endif
      /*  */
      __kmp_depnode_list_free(thread,info->last_ins);
      info->last_ins = NULL;

    } else if ( info->last_out  ) { /* {w|cw}a{wr} dependence */
#if LIBOMP_USE_CONCURRENT_WRITE
        if (!IS_CONCURRENTWRITE(dep->flags) || !IS_CONCURRENT(info->flags, dep->flags))
#endif
        {
          for ( kmp_depnode_list_t * p = info->last_out; p; p = p->next )
          {
            kmp_depnode_t * outdep = p->node;
#if LIBOMP_USE_PARALLEL_SPAWN
            if (outdep ==0) continue; 
#endif

            if ( outdep->dn.task ) {
              KMP_ACQUIRE_DEPNODE(gtid,outdep);
              if ( outdep->dn.task ) {
                __kmp_track_dependence(gtid, outdep, node, task);
                outdep->dn.successors =
                    __kmp_add_node(thread, outdep->dn.successors, node);
#if LIBOMP_USE_REORDER4LOCALITY
                kaapi_reorder4locality_addDependency(
                  thread->th.th_tasklist,
                  KMP_TASK_TO_TASKDATA(outdep->dn.task),
                  KMP_TASK_TO_TASKDATA(task),
                  dep
                );
#endif
                KA_TRACE(
                    40,
                    ("__kmp_process_deps<%d>: T#%d adding dependence from %p to %p\n",
                     filter, gtid, KMP_TASK_TO_TASKDATA(outdep->dn.task),
                     KMP_TASK_TO_TASKDATA(task)));
                npredecessors++;
              }
              KMP_RELEASE_DEPNODE(gtid,outdep);
            }
          }
        }
      }

    if (dep_barrier) {
      // if this is a sync point in the serial sequence, then the previous
      // outputs are guaranteed to be completed after
      // the execution of this task so the previous output nodes can be cleared.
      if (info->last_out) __kmp_depnode_list_free(thread, info->last_out);
      info->last_out = NULL;
    } else {
#if LIBOMP_USE_CONCURRENT_WRITE
      if ( IS_CONCURRENTWRITE(dep->flags) && IS_CONCURRENT(info->flags, dep->flags) )
        info->last_out = __kmp_add_node(thread, info->last_out, node );
      else
#endif
      if (IS_WRITE(dep->flags)) {
#if LIBOMP_USE_PARALLEL_SPAWN
        __kmp_acquire_lock(&thread->th.th_team->t.t_single_lock, gtid);
#endif
        if (info->last_out) __kmp_depnode_list_free(thread, info->last_out);
        info->last_out = __kmp_add_node(thread, NULL, node );//__kmp_node_ref(node);
#if LIBOMP_USE_CONCURRENT_WRITE
        info->flags    = dep->flags;
#endif
#if LIBOMP_USE_PARALLEL_SPAWN
        __kmp_release_lock(&thread->th.th_team->t.t_single_lock, gtid);
#endif
      } else
        info->last_ins = __kmp_add_node(thread, info->last_ins, node);
    }
  }

  KA_TRACE(30, ("__kmp_process_deps<%d>: T#%d found %d predecessors\n", filter,
                gtid, npredecessors));

  return npredecessors;
}

#define NO_DEP_BARRIER (false)
#define DEP_BARRIER (true)

// returns true if the task has any outstanding dependence
static bool __kmp_check_deps(kmp_int32 gtid, kmp_depnode_t *node,
                             kmp_task_t *task, kmp_dephash_t* hash,
                             bool dep_barrier, kmp_int32 ndeps,
                             kmp_depend_info_t *dep_list,
                             kmp_int32 ndeps_noalias,
                             kmp_depend_info_t *noalias_dep_list) {
  int i;
  kmp_info_t *thread = __kmp_threads[ gtid ];
#if KMP_DEBUG
  kmp_taskdata_t *taskdata = KMP_TASK_TO_TASKDATA(task);
#endif
  KA_TRACE(20, ("__kmp_check_deps: T#%d checking dependencies for task %p : %d "
                "possibly aliased dependencies, %d non-aliased depedencies : "
                "dep_barrier=%d .\n",
                gtid, taskdata, ndeps, ndeps_noalias, dep_barrier));

  // Filter deps in dep_list
  // TODO: Different algorithm for large dep_list ( > 10 ? )
  for ( i = 0; i < ndeps; i ++ ) {
#if LIBOMP_USE_VARDEP && OMP_40_ENABLED
    if (dep_list[i].flags.alias == 0)
    {
#if LIBOMP_USE_CONCURRENT_WRITE
      if (dep_list[i].base_addr == thread->th.th_commute_addr) {
        //printf("Detected commute for %p\n", thread->th.th_commute_addr);
        dep_list[i].flags.cw      = thread->th.th_commute_flag & 0x1;
        dep_list[i].flags.commute = thread->th.th_commute_flag & 0x2;
      }
#endif
      for ( int j = i+1; j < ndeps; j++ )
        if ( dep_list[i].base_addr == dep_list[j].base_addr ) {
          dep_list[i].flags.in |= dep_list[j].flags.in;
          dep_list[i].flags.out |= dep_list[j].flags.out;
          dep_list[i].flags.cw |= dep_list[j].flags.cw;
          dep_list[i].flags.commute |= dep_list[j].flags.commute;
          dep_list[j].flags.alias = 1; // Mark j element as void
        }
      }
#else
    if ( dep_list[i].base_addr != 0 ) {
#if LIBOMP_USE_CONCURRENT_WRITE
      if (dep_list[i].base_addr == thread->th.th_commute_addr) {
        //printf("Detected commute for %p\n", thread->th.th_commute_addr);
        dep_list[i].flags.cw      = thread->th.th_commute_flag & 0x1;
        dep_list[i].flags.commute = thread->th.th_commute_flag & 0x2;
      }
#endif
      for ( int j = i+1; j < ndeps; j++ )
        if ( dep_list[i].base_addr == dep_list[j].base_addr ) {
          dep_list[i].flags.in |= dep_list[j].flags.in;
          dep_list[i].flags.out |= dep_list[j].flags.out;
          dep_list[i].flags.cw |= dep_list[j].flags.cw;
          dep_list[i].flags.commute |= dep_list[j].flags.commute;
          dep_list[j].base_addr = 0; // Mark j element as void
        }
    }
#endif // #if LIBOMP_USE_VARDEP && OMP_40_ENABLED
  }
#if LIBOMP_USE_CONCURRENT_WRITE
  thread->th.th_commute_addr = 0;
#endif

  // doesn't need to be atomic as no other thread is going to be accessing this
  // node just yet.
  // npredecessors is set -1 to ensure that none of the releasing tasks queues
  // this task before we have finished processing all the dependencies
  node->dn.npredecessors = -1;

  // used to pack all npredecessors additions into a single atomic operation at
  // the end
  int npredecessors;

  // Here: it could be possible to insert extra deps without making copy of the whole array of extra dependencies
  npredecessors = __kmp_process_deps<true>(gtid, node, hash, dep_barrier,
                                           ndeps, dep_list, task);
  npredecessors += __kmp_process_deps<false>(gtid, node, hash, dep_barrier,
                                             ndeps_noalias, noalias_dep_list, task);

#if LIBOMP_USE_DYNHASH
  //if (hash->nconflicts && hash->nelements && ((double)hash->nelements >= (0.5*KMP_DEPHASH_SIZE(hash->size))))
#if LIBOMP_USE_PARALLEL_SPAWN
  __kmp_acquire_lock(&thread->th.th_team->t.t_single_lock, gtid);
#endif
  while (hash->nconflicts && ((double)hash->nconflicts >= (0.05*hash->nelements)))
  {
    {
#if 0
      printf("#conflict== %i, #nelts==%i, #size==%i conflict==(%8.2f%)\n", hash->nconflicts, hash->nelements, KMP_DEPHASH_SIZE(hash->size), 100*(double)hash->nconflicts / (double)hash->nelements);
#endif
      size_t oldsize = hash->size;
      __kmp_dephash_resize( thread, hash );
#if 0
      printf("-- resize hash map: %i -> %i\n", KMP_DEPHASH_SIZE(oldsize), KMP_DEPHASH_SIZE(hash->size));
      printf("#conflict== %i, #nelts==%i, #size==%i conflict==(%8.2f%)\n", hash->nconflicts, hash->nelements, KMP_DEPHASH_SIZE(hash->size), 100*(double)hash->nconflicts / (double)hash->nelements);
#endif
    }
  }
#if LIBOMP_USE_PARALLEL_SPAWN
  __kmp_release_lock(&thread->th.th_team->t.t_single_lock, gtid);
#endif
#endif

  node->dn.task = task;
  KMP_MB();

  // Account for our initial fake value
  npredecessors++;

  // Update predecessors and obtain current value to check if there are still
  // any outstandig dependences (some tasks may have finished while we processed
  // the dependences)
  npredecessors = KMP_TEST_THEN_ADD32(&node->dn.npredecessors, npredecessors) +
                  npredecessors;

  KA_TRACE(20, ("__kmp_check_deps: T#%d found %d predecessors for task %p \n",
                gtid, npredecessors, taskdata));

  // beyond this point the task could be queued (and executed) by a releasing
  // task...
  return npredecessors > 0 ? true : false;
}


/*
*/
static void
__kmp_release_deps_node ( kmp_int32 gtid, kmp_depnode_t *node )
{
  kmp_info_t *thread = __kmp_threads[ gtid ];
  if ( !node ) return;

  KA_TRACE(20, ("__kmp_release_deps_node: T#%d notifying succesors of node %p.\n", gtid, node ) );

  kmp_depnode_list_t *next;
  for (kmp_depnode_list_t *p = node->dn.successors; p; p = next) {
    kmp_depnode_t *successor = p->node;
    kmp_int32 npredecessors =
        KMP_TEST_THEN_DEC32(&successor->dn.npredecessors) - 1;

    // successor task can be NULL for wait_depends or because deps are still being processed
    if (npredecessors == 0) {
      KMP_MB();
      if (successor->dn.task)
      {
        KA_TRACE(20, ("__kmp_release_deps: T#%d successor %p of %p scheduled "
                      "for execution.\n",
                      gtid, successor->dn.task, node));
#if LIBOMP_USE_CONCURRENT_WRITE
        if (successor->dn.task == &dummy_cw_node)
        { /* sucessor is a in_node or out_node of a cw depnode_t */
          //printf("Succ: %p of %p ready. Is CW dummy synchronization node\n", successor, node );
          KMP_ACQUIRE_DEPNODE(gtid, successor);
          successor->dn.task = NULL;
          KMP_RELEASE_DEPNODE(gtid, successor);
          __kmp_release_deps_node( gtid, successor );
        }
        else
        {
          /* try to acquired all n-commute arguments, it not possible release all of them */
          if (__kmp_acquired_or_failed( gtid, successor, successor->dn.cw ) ==0)
          {
            //printf("Succ: %p of %p ready and ressources are acquired\n", successor, node );
            __kmp_omp_task(gtid,successor->dn.task,false);
          }
          //else printf("Succ: %p of %p ready but ressource not acquired\n", successor, node );
        }
#else
        __kmp_omp_task(gtid,successor->dn.task,false);
#endif
      }
    }

    next = p->next;
    __kmp_node_deref(thread, p->node);
#if USE_FAST_MEMORY
    __kmp_fast_free(thread, p);
#else
    __kmp_thread_free(thread, p);
#endif
  }
  KA_TRACE(
      20,
      ("__kmp_release_deps: T#%d all successors of %p notified of completion\n",
       gtid, node));
}

/*
*/
void
__kmp_release_deps ( kmp_int32 gtid, kmp_taskdata_t *task )
{
  kmp_info_t *thread = __kmp_threads[ gtid ];
  kmp_depnode_t *node = task->td_depnode;

  if ( task->td_dephash ) {
      KA_TRACE(40, ("__kmp_realease_deps: T#%d freeing dependencies hash of task %p.\n", gtid, task ) );
      __kmp_dephash_free(thread,task->td_dephash);
  }

  if ( !node ) return;

  KA_TRACE(20, ("__kmp_realease_deps: T#%d notifying succesors of task %p.\n", gtid, task ) );

  KMP_ACQUIRE_DEPNODE(gtid,node);
  node->dn.task = NULL; // mark this task as finished, so no new dependencies are generated
  KMP_RELEASE_DEPNODE(gtid,node);

#if LIBOMP_USE_CONCURRENT_WRITE
  /* release commute access lock if any */
  //for ( kmp_depnode_list_t *list_commute = ttask->list_commute; list_commute; list_commute = list_commute->next )
  if (node->dn.cw)
  {
    /* commute: make task ready only if the shared ressource is not alread locked
       tasks are (== graph nodes) enqueued in info->last_waiter and npred is incremented.
       on release all cw will decrease it.
    */
    kmp_cw_depnode_t* c_sync = (kmp_cw_depnode_t*)node->dn.cw; //list_commute->node;
    KMP_ACQUIRE_DEPNODE(gtid, &c_sync->in_node);
    KMP_ASSERT( c_sync->flags.cw || (c_sync->state ==1));
    if (c_sync->flags.commute)
    {
      if (!c_sync->head_waiter)
        c_sync->state = 0;
      else {
        /* wake-up c_sync->accessed remains set to 'clean' */
      }
    }
    --c_sync->nwriters;
    KMP_RELEASE_DEPNODE(gtid, &c_sync->in_node);
  }
  //__kmp_depnode_list_free(thread, ttask->list_commute);
#endif

  __kmp_release_deps_node(gtid, node);
  __kmp_node_deref(thread, node);

  KA_TRACE(
      40,
      ("__kmp_release_deps: T#%d all successors of %p notified. Return\n",
       gtid, task));
}


/*  */
void
__kmpc_omp_set_depend_info(void* base_addr, kmp_uint32 flag)
{
#if LIBOMP_USE_CONCURRENT_WRITE
  kmp_info_t      * thread = __kmp_threads[ __kmp_entry_gtid() ];
  //printf("Setting commute for %p\n", base_addr);
  thread->th.th_commute_addr  = (kmp_intptr_t)base_addr;
  thread->th.th_commute_flag = (0x1|0x2)&flag; /* drop unknown bits */
#endif
  return;
}


#if LIBOMP_USE_VARDEP

/* */
static int kmp_add_extradeps( kmp_info_t *thread, int kind,
                              int mode, int count, void** array)
{
#define OMPEXT_MODE_READ       1
#define OMPEXT_MODE_WRITE      2
#define OMPEXT_MODE_READWRITE  3
#define OMPEXT_MODE_CONCURRENT 4
#define OMPEXT_MODE_ARRAY      (1<<8)

  kmp_extra_depinfo_th_t* bloc;
  if (thread->th.th_edeps_tail[kind] !=0)
    bloc = (kmp_extra_depinfo_th_t*)__kmp_fast_allocate(thread, sizeof(kmp_extra_depinfo_th_t));
  else
    bloc = &thread->th.th_edeps[kind];

  bloc->ed_size    = count;
  bloc->ed_extent  = 0;
  bloc->ed_deps    = (kmp_intptr_t*)array;
  bloc->ed_mode    = (kmp_uint16)mode;
  bloc->ed_noalias = kind;
  bloc->ed_next    = 0;
  if (thread->th.th_edeps_tail[kind] !=0)
    thread->th.th_edeps_tail[kind]->ed_next = bloc;
  thread->th.th_edeps_tail[kind] = bloc;
  thread->th.th_edps_size[kind] += count;
  return 0;
}

/* */
static int kmp_add_extradeps_array( kmp_info_t *thread, int kind,
                                    int mode, int count, int extent, void* array)
{
  kmp_extra_depinfo_th_t* bloc;
  if (thread->th.th_edeps_tail[kind] !=0)
    bloc = (kmp_extra_depinfo_th_t*)__kmp_fast_allocate(thread, sizeof(kmp_extra_depinfo_th_t));
  else
    bloc = &thread->th.th_edeps[kind];

  bloc->ed_size    = count;
  bloc->ed_extent  = extent;
  bloc->ed_deps    = (kmp_intptr_t*)array;
  bloc->ed_mode    = OMPEXT_MODE_ARRAY|(kmp_uint16)mode;
  bloc->ed_noalias = kind;
  bloc->ed_next    = 0;
  if (thread->th.th_edeps_tail[kind] !=0)
    thread->th.th_edeps_tail[kind]->ed_next = bloc;
  thread->th.th_edeps_tail[kind] = bloc;
  thread->th.th_edps_size[kind] += count;
  return 0;
}


/* */
int __kmpc_omp_task_declare_dependencies( ident_t *loc_ref, kmp_int32 gtid,
                                          int mode, int count, void** array)
{
  kmp_info_t *thread = __kmp_threads[ gtid ];
  if (array==0) return EINVAL;
  if (count<0) return EINVAL;
#ifdef KMP_DEBUG
  KMP_ASSERT( !((mode<OMPEXT_MODE_READ)||(mode >OMPEXT_MODE_CONCURRENT)) );
#else
  if ((mode<OMPEXT_MODE_READ)||(mode >OMPEXT_MODE_CONCURRENT))
  {
    printf("***error invalid access mode: %i\n", mode );
    return EINVAL;
  }
#endif
  return kmp_add_extradeps(thread, 0 /* alias */, mode, count, array );
}


/* */
int __kmpc_omp_task_declare_dependencies_noalias( ident_t *loc_ref, kmp_int32 gtid,
                                                  int mode, int count, void** array)
{
  kmp_info_t *thread = __kmp_threads[ gtid ];
  if (array==0) return EINVAL;
  if (count<0) return EINVAL;
#ifdef KMP_DEBUG
  KMP_ASSERT( !((mode<OMPEXT_MODE_READ)||(mode >OMPEXT_MODE_CONCURRENT)) );
#else
  if ((mode<OMPEXT_MODE_READ)||(mode >OMPEXT_MODE_CONCURRENT))
  {
    printf("***error invalid access mode: %i\n", mode );
    return EINVAL;
  }
#endif
  return kmp_add_extradeps(thread, 1 /* no alias */, mode, count, array );
}

int __kmpc_omp_task_declare_dependencies_array( ident_t *loc_ref, kmp_int32 gtid,
                                          int mode, int count, int extent, void* array)
{
  kmp_info_t *thread = __kmp_threads[ gtid ];
  if (array==0) return EINVAL;
  if (count<0) return EINVAL;
#ifdef KMP_DEBUG
  KMP_ASSERT( !((mode<OMPEXT_MODE_READ)||(mode >OMPEXT_MODE_CONCURRENT)) );
#else
  if ((mode<OMPEXT_MODE_READ)||(mode >OMPEXT_MODE_CONCURRENT))
  {
    printf("***error invalid access mode: %i\n", mode );
    return EINVAL;
  }
#endif
  return kmp_add_extradeps_array(thread, 0 /* alias */, mode, count, extent, array );
}


/* */
int __kmpc_omp_task_declare_dependencies_array_noalias( ident_t *loc_ref, kmp_int32 gtid,
                                                  int mode, int count, int extent, void* array)
{
  kmp_info_t *thread = __kmp_threads[ gtid ];
  if (array==0) return EINVAL;
  if (count<0) return EINVAL;
#ifdef KMP_DEBUG
  KMP_ASSERT( !((mode<OMPEXT_MODE_READ)||(mode >OMPEXT_MODE_CONCURRENT)) );
#else
  if ((mode<OMPEXT_MODE_READ)||(mode >OMPEXT_MODE_CONCURRENT))
  {
    printf("***error invalid access mode: %i\n", mode );
    return EINVAL;
  }
#endif
  return kmp_add_extradeps_array(thread, 1 /* no alias */, mode, count, extent, array );
}

#endif


/*!
@ingroup TASKING
@param loc_ref location of the original task directive
@param gtid Global Thread ID of encountering thread
@param new_task task thunk allocated by __kmp_omp_task_alloc() for the ''new
task''
@param ndeps Number of depend items with possible aliasing
@param dep_list List of depend items with possible aliasing
@param ndeps_noalias Number of depend items with no aliasing
@param noalias_dep_list List of depend items with no aliasing

@return Returns either TASK_CURRENT_NOT_QUEUED if the current task was not
suspendend and queued, or TASK_CURRENT_QUEUED if it was suspended and queued

Schedule a non-thread-switchable task with dependences for execution
*/
kmp_int32 __kmpc_omp_task_with_deps(ident_t *loc_ref, kmp_int32 gtid,
                                    kmp_task_t *new_task, kmp_int32 ndeps,
                                    kmp_depend_info_t *dep_list,
                                    kmp_int32 ndeps_noalias,
                                    kmp_depend_info_t *noalias_dep_list) {

  kmp_taskdata_t *new_taskdata = KMP_TASK_TO_TASKDATA(new_task);
  KA_TRACE(10, ("__kmpc_omp_task_with_deps(enter): T#%d loc=%p task=%p\n", gtid,
                loc_ref, new_taskdata));

  kmp_info_t *thread = __kmp_threads[gtid];
  kmp_taskdata_t *current_task = thread->th.th_current_task;

  /* Keep copy of dependencies in task
     - allow to runtime discovery of task dependencies (for performance monitoring for instance)
     or debugging scheduler. 
  */
//    KMP_ASSERT(
//       (new_taskdata->td_ndeps+new_taskdata->td_ndeps_noalias ==0) ||
//       (new_taskdata->td_ndeps+new_taskdata->td_ndeps_noalias == ndeps+ndeps_noalias)
//    );

#if LIBOMP_USE_VARDEP
  /* - all deps including extra deps are allocated into task data struct if depsinalloc is set */
  /* - ndeps and ndeps_noalias does not take into account extra deps */
  /* - if depsfill == 1 (i.e. gcc) then deps are already copied into taskdata->td_deps array */
  kmp_int32 ndeps_extra = thread->th.th_edps_size[0];
  kmp_int32 ndeps_extra_noalias = thread->th.th_edps_size[1];
  kmp_int32 total_ndeps = ndeps + ndeps_extra;
  kmp_int32 total_ndeps_noalias = ndeps_noalias + ndeps_extra_noalias;
#else
  kmp_int32 total_ndeps = ndeps;
  kmp_int32 total_ndeps_noalias = ndeps_noalias;
#endif
  /* allocate here if not done or not enough (+extra deps) in task_alloc */
  if (new_taskdata->td_ndeps+new_taskdata->td_ndeps_noalias < total_ndeps+total_ndeps_noalias)
  {
    kmp_depend_info_t* td_deps = (kmp_depend_info_t*)__kmp_fast_allocate( thread,
        (total_ndeps + total_ndeps_noalias)*sizeof(kmp_depend_info_t)
    );
    new_taskdata->td_flags.depsinalloc = 0;
    if (new_taskdata->td_flags.depsfill)
      KMP_MEMCPY( td_deps, new_taskdata->td_deps, ndeps * sizeof(kmp_depend_info_t) );
    if (new_taskdata->td_deps)
      __kmp_fast_free(thread, new_taskdata->td_deps);
    // do not free td_deps_noalias, always in same allocated bloc
    new_taskdata->td_deps = td_deps;
    new_taskdata->td_deps_noalias = td_deps+total_ndeps;
  }

  kmp_int32 i;
  kmp_depend_info_t* deps = new_taskdata->td_deps;
  if (new_taskdata->td_flags.depsfill ==0) /* gcc already have stored dependencies */
  {
    /* if not allocated */
    if (deps != dep_list)
      for (i=0; i<ndeps; ++i) {
        dep_list[i].flags.alias = 0;
        *deps++ = dep_list[i];
      }
    else
      deps += ndeps;
  }
  else
    deps += ndeps;

#if LIBOMP_USE_VARDEP
  /* deps with possibly alias */
  kmp_extra_depinfo_th_t* bloc  = &thread->th.th_edeps[0];
  if (thread->th.th_edps_size[0]) 
  do {
    if (bloc->ed_size) {
      kmp_depend_info_flags_t flags;
      flags.in = 1; //(bloc->ed_mode & OMPEXT_MODE_READ ? 1: 0);
      flags.out= (bloc->ed_mode & OMPEXT_MODE_WRITE ? 1: 0);
      flags.cw = (bloc->ed_mode & OMPEXT_MODE_CONCURRENT ? 1: 0);
      flags.commute = 0;
      flags.alias = 0;
      for (i=0; i<bloc->ed_size; ++i, ++deps)
      {
        if (bloc->ed_mode & OMPEXT_MODE_ARRAY)
          deps->base_addr = (kmp_intptr_t)(bloc->ed_extent*i+(unsigned char*)bloc->ed_deps);
        else
          deps->base_addr = bloc->ed_deps[i];
        deps->len       = 1;
        deps->flags     = flags;
      }
    }
    kmp_extra_depinfo_th_t* blocn = bloc->ed_next;
    if (bloc != &thread->th.th_edeps[0])
      __kmp_fast_free(thread, bloc );
    bloc = blocn;
  } while (bloc != 0);
#endif
  KMP_ASSERT( deps == new_taskdata->td_deps_noalias || (total_ndeps_noalias==0) );
  for (i=0; i<ndeps_noalias; ++i) {
    noalias_dep_list[i].flags.alias = 0;
    *deps++ = noalias_dep_list[i];
  }
#if LIBOMP_USE_VARDEP
  bloc  = &thread->th.th_edeps[1];
  if (thread->th.th_edps_size[1]) 
  do {
    if (bloc->ed_size) {
      kmp_depend_info_flags_t flags;
      flags.in = 1; //(bloc->ed_mode & OMPEXT_MODE_READ ? 1: 0);
      flags.out= (bloc->ed_mode & OMPEXT_MODE_WRITE ? 1: 0);
      flags.cw = (bloc->ed_mode & OMPEXT_MODE_CONCURRENT ? 1: 0);
      flags.commute = 0;
      flags.alias = 0;
      for (i=0; i<bloc->ed_size; ++i, ++deps)
      {
        if (bloc->ed_mode & OMPEXT_MODE_ARRAY)
          deps->base_addr = (kmp_intptr_t)(bloc->ed_extent*i+(unsigned char*)bloc->ed_deps);
        else
          deps->base_addr = bloc->ed_deps[i];
        deps->len       = 1;
        deps->flags     = flags;
      }
    }
    kmp_extra_depinfo_th_t* blocn = bloc->ed_next;
    if (bloc != &thread->th.th_edeps[1])
      __kmp_fast_free(thread, bloc );
    bloc = blocn;
  } while (bloc != 0);
  /* reset thread state about extra deps */
  thread->th.th_edps_size[0] =0;
  thread->th.th_edps_size[1] =0;
  thread->th.th_edeps_tail[0] =0;
  thread->th.th_edeps_tail[1] =0;
#endif

//kmp_depend_info_t *save_dep_list = dep_list;
  dep_list = new_taskdata->td_deps;
  ndeps    = new_taskdata->td_ndeps = total_ndeps;
  noalias_dep_list = new_taskdata->td_deps_noalias;
  ndeps_noalias    = new_taskdata->td_ndeps_noalias = total_ndeps_noalias;

  bool serial = current_task->td_flags.team_serial ||
                current_task->td_flags.tasking_ser ||
                current_task->td_flags.final;
#if OMP_45_ENABLED
  kmp_task_team_t *task_team = thread->th.th_task_team;
  serial = serial && !(task_team && task_team->tt.tt_found_proxy_tasks);
#endif

    if (!serial && (ndeps > 0 || ndeps_noalias > 0
#if LIBOMP_USE_VARDEP
                              || ndeps_extra >0
                              || ndeps_extra_noalias >0
#endif
    )) {
    /* if no dependencies have been tracked yet, create the dependence hash */
    kmp_dephash_t* hash;
#if LIBOMP_USE_PARALLEL_SPAWN
    hash = thread->th.th_team->t.t_implicit_task_taskdata[0].td_dephash;
#else
    hash = current_task->td_dephash;
#endif
    if (hash== NULL )
    {
#if LIBOMP_USE_PARALLEL_SPAWN
      __kmp_acquire_lock(&thread->th.th_team->t.t_single_lock, gtid);
      hash = thread->th.th_team->t.t_implicit_task_taskdata[0].td_dephash;
      if (hash== NULL )
#endif
      {
        hash = __kmp_dephash_create(thread, current_task);
#if LIBOMP_USE_PARALLEL_SPAWN
        thread->th.th_team->t.t_implicit_task_taskdata[0].td_dephash = hash;
#else
        current_task->td_dephash = hash;
#endif
      }
#if LIBOMP_USE_PARALLEL_SPAWN
      __kmp_release_lock(&thread->th.th_team->t.t_single_lock, gtid);
#endif
    }


#if USE_FAST_MEMORY
    kmp_depnode_t *node =
        (kmp_depnode_t *)__kmp_fast_allocate(thread, sizeof(kmp_depnode_t));
#else
    kmp_depnode_t *node =
        (kmp_depnode_t *)__kmp_thread_malloc(thread, sizeof(kmp_depnode_t));
#endif

    __kmp_init_node(node);
    new_taskdata->td_depnode = node;

    if ( __kmp_check_deps( gtid, node, new_task, hash, NO_DEP_BARRIER,
                           ndeps, dep_list,
                           ndeps_noalias,noalias_dep_list ) ) {
      KA_TRACE(10, ("__kmpc_omp_task_with_deps(exit): T#%d task had blocking "
                    "dependencies: "
                    "loc=%p task=%p, return: TASK_CURRENT_NOT_QUEUED\n",
                    gtid, loc_ref, new_taskdata));
#if 0
      printf("%i:: Task %p #deps=%i @dep=%p/%p @=%p/%p has dependencies.\n", gtid, new_task, ndeps, dep_list, save_dep_list, 
                   (ndeps >0 ? dep_list[0].base_addr : 0), 
                   (ndeps >0 ? save_dep_list[0].base_addr : 0) 
      );
#endif
      return TASK_CURRENT_NOT_QUEUED;
    }
    else {
#if 0
      printf("%i:: Task %p #deps=%i @dep=%p/%p @=%p/%p is independent.\n", gtid, new_task, ndeps, dep_list, save_dep_list, 
                   (ndeps >0 ? dep_list[0].base_addr : 0), 
                   (ndeps >0 ? save_dep_list[0].base_addr : 0) 
      );
#endif
    }
#if LIBOMP_USE_CONCURRENT_WRITE
    /* task ready: check to acquired commute ressources */
    if ((node->dn./*list_*/cw) && __kmp_acquired_or_failed( gtid, node, node->dn./*list_*/cw) )
        return TASK_CURRENT_NOT_QUEUED;
    /* task ready + commute ressources acquired: push it */
#endif
  } else {
    KA_TRACE(10, ("__kmpc_omp_task_with_deps(exit): T#%d ignored dependencies "
                  "for task (serialized)"
                  "loc=%p task=%p\n",
                  gtid, loc_ref, new_taskdata));
  }

  KA_TRACE(10, ("__kmpc_omp_task_with_deps(exit): T#%d task had no blocking "
                "dependencies : "
                "loc=%p task=%p, transferring to __kmpc_omp_task\n",
                gtid, loc_ref, new_taskdata));

//TG: do not call the c entry point that may check vardep to call kmpc_omp_task_with_deps
// generating infinite recursive function call.
//    return __kmpc_omp_task(loc_ref, gtid, new_task);
  return __kmp_omp_task(gtid, new_task, false); // TG: it was a true ???
}

/*!
@ingroup TASKING
@param loc_ref location of the original task directive
@param gtid Global Thread ID of encountering thread
@param ndeps Number of depend items with possible aliasing
@param dep_list List of depend items with possible aliasing
@param ndeps_noalias Number of depend items with no aliasing
@param noalias_dep_list List of depend items with no aliasing

Blocks the current task until all specifies dependencies have been fulfilled.
*/
void __kmpc_omp_wait_deps(ident_t *loc_ref, kmp_int32 gtid, kmp_int32 ndeps,
                          kmp_depend_info_t *dep_list, kmp_int32 ndeps_noalias,
                          kmp_depend_info_t *noalias_dep_list) {
  KA_TRACE(10, ("__kmpc_omp_wait_deps(enter): T#%d loc=%p\n", gtid, loc_ref));

  if (ndeps == 0 && ndeps_noalias == 0) {
    KA_TRACE(10, ("__kmpc_omp_wait_deps(exit): T#%d has no dependencies to "
                  "wait upon : loc=%p\n",
                  gtid, loc_ref));
    return;
  }

  kmp_info_t *thread = __kmp_threads[gtid];
  kmp_taskdata_t *current_task = thread->th.th_current_task;

  // We can return immediately as:
  // - dependences are not computed in serial teams (except with proxy tasks)
  // - if the dephash is not yet created it means we have nothing to wait for
  bool ignore = current_task->td_flags.team_serial ||
                current_task->td_flags.tasking_ser ||
                current_task->td_flags.final;
#if OMP_45_ENABLED
  ignore = ignore && thread->th.th_task_team != NULL &&
           thread->th.th_task_team->tt.tt_found_proxy_tasks == FALSE;
#endif
  ignore = ignore || current_task->td_dephash == NULL;

  if (ignore) {
    KA_TRACE(10, ("__kmpc_omp_wait_deps(exit): T#%d has no blocking "
                  "dependencies : loc=%p\n",
                  gtid, loc_ref));
    return;
  }

  kmp_depnode_t node;
  __kmp_init_node(&node);

  kmp_dephash_t* hash;
  hash = current_task->td_dephash;
  hash = thread->th.th_team->t.t_implicit_task_taskdata[0].td_dephash;
  if (!__kmp_check_deps(gtid, &node, NULL, hash,
                        DEP_BARRIER, ndeps, dep_list, ndeps_noalias,
                        noalias_dep_list)) {
    KA_TRACE(10, ("__kmpc_omp_wait_deps(exit): T#%d has no blocking "
                  "dependencies : loc=%p\n",
                  gtid, loc_ref));
    return;
  }

  int thread_finished = FALSE;
  kmp_flag_32 flag((volatile kmp_uint32 *)&(node.dn.npredecessors), 0U);
  while (node.dn.npredecessors > 0) {
    flag.execute_tasks(thread, gtid, FALSE, &thread_finished,
#if USE_ITT_BUILD
                       NULL,
#endif
                       __kmp_task_stealing_constraint);
  }

  KA_TRACE(10, ("__kmpc_omp_wait_deps(exit): T#%d finished waiting : loc=%p\n",
                gtid, loc_ref));
}

#endif /* OMP_40_ENABLED */
