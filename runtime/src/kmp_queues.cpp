/*
 * kmp_queues.cpp -- machine management
 */


#include "kmp_queues.h"
#include "kmp_hws.h"

//TODO GUARD this whole file


int __kmp_cpu2node(int cpu) { return machine_info.cpu2node[cpu]; }


void __kmp_init_task_deque(kmp_queue_data_t *q, int level, int level_id, int node)
{
    kmp_base_queue_data_t *queue = &q->qd;
#if LIBOMP_USE_THEQUEUE || LIBOMP_USE_LINKED_DEQUEUE
    kaapi_wsqueue_init( &queue->td_wsdeque, INITIAL_TASK_DEQUE_SIZE, node);
#else
    __kmp_alloc_task_deque(queue, node);
#endif
    queue->td_level = level;
    queue->td_id    = level_id;
}


//------------------------------------------------------------------------------
// __kmp_alloc_task_deque:
// Allocates a task deque for a particular thread, and initialize the necessary
// data structures relating to the deque.  This only happens once per thread
// per task team since task teams are recycled.
// No lock is needed during allocation since each thread allocates its own
// deque.

void
__kmp_alloc_task_deque(kmp_base_queue_data_t *queue, int node)
{
#if LIBOMP_USE_THEQUEUE || LIBOMP_USE_LINKED_DEQUEUE
    kaapi_wsqueue_init( &queue->td_wsdeque, INITIAL_TASK_DEQUE_SIZE, node);
#else
    /*TODO PV something? I need to allocate node queue on nodes...*/
    __kmp_init_bootstrap_lock( &queue->td_deque_lock );
    KMP_DEBUG_ASSERT( queue->td_deque == NULL );

    // Initialize last stolen task field to "none"
    queue->td_deque_last_stolen = -1;

    KMP_DEBUG_ASSERT( TCR_4(queue->td_deque_ntasks) == 0 );
    KMP_DEBUG_ASSERT( queue->td_deque_head == 0 );
    KMP_DEBUG_ASSERT( queue->td_deque_tail == 0 );

    KE_TRACE( 10, ( "__kmp_alloc_task_deque: allocating deque[%d]\n", INITIAL_TASK_DEQUE_SIZE ) );
    // Allocate space for task deque, and zero the deque
    // Cannot use __kmp_thread_calloc() because threads not around for
    // kmp_reap_task_team( ).
    queue->td_deque = (kmp_taskdata_t **)
#if LIBOMP_USE_AFFINITY
        numa_alloc_onnode( INITIAL_TASK_DEQUE_SIZE * sizeof(kmp_taskdata_t *), node);
    queue->td_deque_node = node;
#else
        __kmp_allocate( INITIAL_TASK_DEQUE_SIZE * sizeof(kmp_taskdata_t *));
#endif
    queue->td_deque_size = INITIAL_TASK_DEQUE_SIZE;
    queue->td_deque_last_stolen = -1;
#endif
}

//------------------------------------------------------------------------------
// __kmp_realloc_task_deque:
// Re-allocates a task deque for a particular thread, copies the content from the old deque
// and adjusts the necessary data structures relating to the deque.
// This operation must be done with a the deque_lock being held

void __kmp_realloc_task_deque(kmp_base_queue_data_t *queue)
{
#if LIBOMP_USE_THEQUEUE || LIBOMP_USE_LINKED_DEQUEUE
    kaapi_wsqueue_realloc( &queue->td_wsdeque );
#else
    kmp_int32 size = TASK_DEQUE_SIZE(queue);
    kmp_int32 new_size = 2 * size;
    kmp_taskdata_t ** new_deque;

    KE_TRACE( 10, ( "__kmp_realloc_task_deque: reallocating deque[from %d to %d]\n", size, new_size ) );


    int i;
    new_deque = (kmp_taskdata_t **)
#if LIBOMP_USE_AFFINITY
        numa_alloc_onnode( new_size * sizeof(kmp_taskdata_t *), queue->td_deque_node);
#else
        __kmp_allocate( new_size * sizeof(kmp_taskdata_t *));
#endif
    int j;
    for ( i = queue->td_deque_head, j = 0; j < size; i = (i+1) & TASK_DEQUE_MASK(queue), j++ )
       new_deque[j] = queue->td_deque[i];

#if LIBOMP_USE_AFFINITY
    numa_free((void*)queue->td_deque, size);
#else
    __kmp_free((void*)queue->td_deque);
#endif
    queue->td_deque = new_deque;
    queue->td_deque_size = new_size;
    queue->td_deque_head = 0;
    queue->td_deque_tail = size;
#endif
}

//------------------------------------------------------------------------------
// __kmp_free_task_deque:
// Deallocates a task deque for a particular thread.
// Happens at library deallocation so don't need to reset all thread data fields.

void
__kmp_free_task_deque( kmp_base_queue_data_t *own_queue )
{
#if LIBOMP_USE_THEQUEUE || LIBOMP_USE_LINKED_DEQUEUE
    kaapi_wsqueue_fini( &own_queue->td_wsdeque );
#else
    __kmp_acquire_bootstrap_lock( &own_queue->td_deque_lock );

    if ( own_queue->td_deque != NULL ) {
        TCW_4(own_queue->td_deque_ntasks, 0);
#if LIBOMP_USE_AFFINITY
        numa_free((void*)own_queue->td_deque, TASK_DEQUE_SIZE(own_queue));
#else
        __kmp_free((void*)own_queue->td_deque);
#endif
        own_queue->td_deque = NULL;
    }
    __kmp_release_bootstrap_lock( &own_queue->td_deque_lock );
#endif
}
