/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** vincent.danjean@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_WSPROTOCOL_H
#define _KAAPI_WSPROTOCOL_H 1

/* FWD
*/
struct kaapi_request_node_t;
struct kaapi_listrequest_iterator_t;

/* Map kmp taskdata to kaapi_task
*/
typedef struct kmp_taskdata kaapi_task_t;

/* ============================= TASK ============================ */
/* Mix of Kaapi workqueue with its aggregation protocol based of CC sync
   If LIBOMP_USE_THE_AGGREGATION !=1 use aggregation protocol, else use 
   classical T.H.E algorithm
*/
typedef struct kaapi_wsqueue_t {
#if LIBOMP_USE_LINKED_DEQUEUE
#if !LIBOMP_USE_THE_AGGREGATION
    kmp_bootstrap_lock_t    deque_lock;                 // Lock for accessing deque
#endif
    kaapi_task_t *          deque_H;
    kaapi_task_t *          deque_T;
    kmp_int32               deque_size;                 // Number of tasks in deque
    int                     numa_node;                  // Prefered numa node or -1 if unspecified

#else // !LIBOMP_USE_LINKED_DEQUEUE

    volatile unsigned int   deque_H;                    // Head of deque: steal by the thief
#if !LIBOMP_USE_THE_AGGREGATION
    kmp_bootstrap_lock_t    deque_lock;                 // Lock for accessing deque: always taken by the thieves
#endif
    volatile unsigned int   deque_T KMP_ALIGN_CACHE;    // Tail of deque: push/pop by the owner
#if !LIBOMP_USE_THE_AGGREGATION
    kmp_bootstrap_lock_t    deque_lock_owner;           // Lock for accessing deque by the owners (pop + push)
#endif
    int                     numa_node;                  // Prefered numa node or -1 if unspecified
    kaapi_task_t**          deque;                      // Deque of tasks encountered by td_thr, dynamically allocated
    unsigned int            deque_size;                 // Size of deck
#endif
#if LIBOMP_USE_THE_AGGREGATION
    struct kaapi_request_node_t* tail;
#endif
} kaapi_wsqueue_t;
  

/*
*/
static inline int kaapi_wsqueue_empty( kaapi_wsqueue_t* queue )
{
#if LIBOMP_USE_LINKED_DEQUEUE
  return queue->deque_H == 0;
#else
  return queue->deque_H >= queue->deque_T;
#endif
}

extern int kaapi_wsqueue_init(kaapi_wsqueue_t* queue, size_t size, int numa_node );
extern int kaapi_wsqueue_fini(kaapi_wsqueue_t* queue);
extern int kaapi_wsqueue_realloc(kaapi_wsqueue_t* queue );

/* push
   If remote = 0, must be called by the owner of the queue only (one thread)
   If remote = 1, the task is pushed such that the next steal request will get it.
   Return 0 in case of success else return ENOMEM if it cannot push
*/
extern int kaapi_wsqueue_push_task(kaapi_wsqueue_t* queue, int remote, kaapi_task_t* task );

/* same as kaapi_wsqueue_push_task - assume no concurrency
*/
extern int __kaapi_wsqueue_push_task(kaapi_wsqueue_t* queue, int remote, kaapi_task_t* task );

/* push list of task
   Return 0 in case of success else return ENOMEM if it cannot push
*/
#if LIBOMP_USE_LINKED_DEQUEUE
extern int kaapi_wsqueue_push_tasklist(kaapi_wsqueue_t* dest, kaapi_wsqueue_t* src );
extern int __kaapi_wsqueue_push_tasklist(kaapi_wsqueue_t* queue, kaapi_wsqueue_t* list );
#endif

/* push
   Serialize owner(s) before pushing task.
   If remote = 1, the task is pushed such that the next steal request will get it.
   Return 0 in case of success else return ENOMEM if it cannot push
*/
extern int kaapi_wsqueue_locked_push_task(kaapi_wsqueue_t* queue, int remote, kaapi_task_t* task );

/* pop
   Must be called by the owner of the queue only (one onwer).
   Return 0 in case of failure
*/
extern kaapi_task_t* kaapi_wsqueue_pop_task( kaapi_wsqueue_t* queue );

/* same as kaapi_wsqueue_pop_task - assume no concurrency
*/
extern kaapi_task_t* __kaapi_wsqueue_pop_task( kaapi_wsqueue_t* queue );


/* pop
   May be called by owner threads of the queue.
   Return 0 in case of failure
*/
extern kaapi_task_t* kaapi_wsqueue_locked_pop_task( kaapi_wsqueue_t* queue );

/* pop
   Must be called by the thieves of the queue.
   Return 0 in case of failure
*/
extern kaapi_task_t* kaapi_wsqueue_steal_task( kaapi_wsqueue_t* queue );

/* same as kaapi_wsqueue_steal_task - assume no concurrency
*/
extern kaapi_task_t* __kaapi_wsqueue_steal_task( kaapi_wsqueue_t* queue );


/* Similar to std::list::splice
   Move tasks between [itBegin,itEnd] just after the position itInsert.
   Currently not yet accessible through the aggregation protocol and could only be used on
   non sharing queue.
*/
extern void kaapi_wsqueue_splice(
      kaapi_wsqueue_t* ready_queue,
      kaapi_task_t* itInsert,
      kaapi_task_t* itBegin,
      kaapi_task_t* itEnd );


#endif
