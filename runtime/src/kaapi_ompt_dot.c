/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <limits.h>
#include "ompt.h"
#include "kmp.h"


/* Use OMPT to generate DOT
*/
static ompt_get_task_id_t ompt_get_task_id;
static ompt_get_thread_id_t ompt_get_thread_id;
static ompt_get_parallel_id_t ompt_get_parallel_id;
FILE* fout = 0;

static void
on_ompt_event_barrier_begin(
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
  printf("%" PRIu64 ": ompt_event_barrier_begin: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", ompt_get_thread_id(), parallel_id, task_id);
}

static void
on_ompt_event_barrier_end(
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
  printf("%" PRIu64 ": ompt_event_barrier_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", ompt_get_thread_id(), parallel_id, task_id);
}

static void
on_ompt_event_initial_task_begin(
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
  printf("%" PRIu64 ": ompt_event_initial_task_begin: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", ompt_get_thread_id(), parallel_id, task_id);
}

static void
on_ompt_event_initial_task_end(
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
  printf("%" PRIu64 ": ompt_event_initial_task_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", ompt_get_thread_id(), parallel_id, task_id);
}


static void
on_ompt_event_implicit_task_begin(
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
  fprintf(fout, "%" PRIu64 "[label=\"%" PRIu64 "\\n implicit\",color=orange,shape=doublecircle,style=filled];\n",
    task_id,
    task_id
  );
}

static void
on_ompt_event_implicit_task_end(
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
}


static void
on_ompt_event_task_begin(
  ompt_task_id_t parent_task_id,     /* id of parent task            */
  ompt_frame_t  *parent_task_frame,  /* frame data for parent task   */
  ompt_task_id_t new_task_id,        /* id of created task           */
  void *task_function,               /* pointer to outlined function */
  ompt_ident_t* loc
)
{
  fprintf(fout, "%" PRIu64 "[label=\"%" PRIu64 "\\n %s\",color=orange,shape=ellipse,style=filled];\n",
    new_task_id,
    new_task_id,
    loc == 0 ? "<undef>" : loc->psource
  );
  fprintf(fout, "%" PRIu64 " -> %" PRIu64 "[style=dotted];\n",
    parent_task_id, new_task_id);
}

static void
on_ompt_event_task_end(
  ompt_task_id_t task_id
)
{
}

static void
on_ompt_event_task_switch(
  ompt_task_id_t first_task_id,
  ompt_task_id_t second_task_id
)
{
}


static void
on_ompt_event_task_dependences(
    ompt_task_id_t task_id,            /* ID of task with dependences */
    const ompt_task_dependence_t *deps,/* vector of task dependences  */
    int ndeps                          /* number of dependences       */
)
{
  for (int i=0; i<ndeps; ++i)
  {
    fprintf(fout, "%" PRIu64 "[label=\"%p\", shape=box,color=steelblue,style=filled];\n",
      (uint64_t)deps[i].variable_addr,
      deps[i].variable_addr
    );
    switch (deps[i].dependence_flags) {
      case ompt_task_dependence_type_out:
        fprintf(fout, "%" PRIu64 " -> %" PRIu64 ";\n",
          task_id, (uint64_t)deps[i].variable_addr
        );
      break;
      case ompt_task_dependence_type_in:
        fprintf(fout, "%" PRIu64 " -> %" PRIu64 ";\n",
          (uint64_t)deps[i].variable_addr, task_id
        );
      break;
      case ompt_task_dependence_type_inout:
        fprintf(fout, "%" PRIu64 " -> %" PRIu64 "[dir=both];\n", task_id, (uint64_t)deps[i].variable_addr);
      default:
      break;
    }
  }
}


static void
on_ompt_event_task_dependence_pair(
  ompt_task_id_t first_task_id,
  ompt_task_id_t second_task_id
)
{
  fprintf(fout, "%" PRIu64 " -> %" PRIu64 ";\n", first_task_id, second_task_id);
}


static void
on_ompt_event_loop_begin(
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t parent_task_id,
  void *workshare_function)
{
  //printf("%" PRIu64 ": ompt_event_loop_begin: parallel_id=%" PRIu64 ", parent_task_id=%" PRIu64 ", workshare_function=%p\n", ompt_get_thread_id(), parallel_id, parent_task_id, workshare_function);
}

static void
on_ompt_event_loop_end(
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id)
{
  //printf("%" PRIu64 ": ompt_event_loop_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", ompt_get_thread_id(), parallel_id, task_id);
}

static void
on_ompt_event_parallel_begin(
  ompt_task_id_t parent_task_id,
  ompt_frame_t *parent_task_frame,
  ompt_parallel_id_t parent_parallel_id,
  ompt_parallel_id_t parallel_id,
  uint32_t requested_team_size,
  void *parallel_function,
  ompt_invoker_t invoker,
  ompt_ident_t* loc)
{
  static int graph_count = 0;
  char filename[128];
  snprintf(filename, 128, "/tmp/graph_%i.dot", ++graph_count);
  fout = fopen( filename, "w");
  fprintf(fout, "digraph G {\n");

//  KAAPI_EVENT_PUSH0( kproc->rsrc.evtkproc, 0, KOMP_EVT_PARINIT_BEG );
//  printf("%" PRIu64 ": ompt_event_parallel_begin: parent_task_id=%" PRIu64 ", parent_task_frame=%p, parallel_id=%" PRIu64 ", requested_team_size=%" PRIu32 ", parallel_function=%p, invoker=%d\n", ompt_get_thread_id(), parent_task_id, parent_task_frame, parallel_id, requested_team_size, parallel_function, invoker);
}

static void
on_ompt_event_parallel_end(
  ompt_parallel_id_t parallel_id,
  ompt_task_id_t task_id,
  ompt_invoker_t invoker)
{
  fprintf(fout, "}\n");
  fclose( fout );
  fout = 0;
}


void kaapi_ompt_initialize(
  ompt_function_lookup_t lookup,
  const char *runtime_version,
  unsigned int ompt_version)
{
  ompt_set_callback_t ompt_set_callback = (ompt_set_callback_t) lookup("ompt_set_callback");
  ompt_get_task_id = (ompt_get_task_id_t) lookup("ompt_get_task_id");
  ompt_get_thread_id = (ompt_get_thread_id_t) lookup("ompt_get_thread_id");
  ompt_get_parallel_id = (ompt_get_parallel_id_t) lookup("ompt_get_parallel_id");


#if 0
  ompt_set_callback(ompt_event_barrier_begin, (ompt_callback_t) &on_ompt_event_barrier_begin);
  ompt_set_callback(ompt_event_barrier_end, (ompt_callback_t) &on_ompt_event_barrier_end);
#endif

#if 0
  ompt_set_callback(ompt_event_initial_task_begin, (ompt_callback_t) &on_ompt_event_initial_task_begin);
  ompt_set_callback(ompt_event_initial_task_end, (ompt_callback_t) &on_ompt_event_initial_task_end);
#endif

#if 1
  ompt_set_callback(ompt_event_implicit_task_begin, (ompt_callback_t) &on_ompt_event_implicit_task_begin);
  ompt_set_callback(ompt_event_implicit_task_end, (ompt_callback_t) &on_ompt_event_implicit_task_end);
#endif

  ompt_set_callback(ompt_event_task_begin, (ompt_callback_t) &on_ompt_event_task_begin);
  ompt_set_callback(ompt_event_task_end, (ompt_callback_t) &on_ompt_event_task_end);
#if 0
  ompt_set_callback(ompt_event_task_switch, (ompt_callback_t) &on_ompt_event_task_switch);
#endif
  ompt_set_callback(ompt_event_task_dependences, (ompt_callback_t) &on_ompt_event_task_dependences);
  ompt_set_callback(ompt_event_task_dependence_pair, (ompt_callback_t) &on_ompt_event_task_dependence_pair);

#if 0
  ompt_set_callback(ompt_event_loop_begin, (ompt_callback_t) &on_ompt_event_loop_begin);
  ompt_set_callback(ompt_event_loop_end, (ompt_callback_t) &on_ompt_event_loop_end);
#endif

  ompt_set_callback(ompt_event_parallel_begin, (ompt_callback_t) &on_ompt_event_parallel_begin);
  ompt_set_callback(ompt_event_parallel_end, (ompt_callback_t) &on_ompt_event_parallel_end);
}


ompt_initialize_t ompt_tool()
{
  printf("[OMP-DOT] GET tool:: ompt-dot ompt_tool ok\n");
  return &kaapi_ompt_initialize;
}


extern
int kaapi_ext_init(void)
{
  printf("[OMP-DOT] RTL:: kaapi_ext_init\n");
  return 0;
}

extern
void kaapi_ext_fini(void)
{
  printf("[OMP-DOT] RTL:: kaapi_ext_fini\n");
}
