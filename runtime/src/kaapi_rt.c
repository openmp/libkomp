/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** fabien.lementec@gmail.com 
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "git_hash.h"
#include "kaapi_error.h"
#include <stdio.h>
#include <stdlib.h>

#if defined(HAVE_CLOCK_GETTIME)
# include <time.h>
#else
# include <sys/time.h>
#endif
#if defined(HAVE_CLOCK_GETTIME)
typedef struct timespec struct_time;
#  define gettime(t) clock_gettime( CLOCK_REALTIME, t)
#  define get_sub_second(t) (1e-9*(double)t.tv_nsec)
#  define get_sub_second_ns(t) ((uint64_t)t.tv_nsec)
#else
typedef struct timeval struct_time;
#  define gettime(t) gettimeofday( t, 0)
#  define get_sub_second(t) (1e-6*(double)t.tv_usec)
#  define get_sub_second_ns(t) (1000*(uint64_t)t.tv_usec)
#endif


#if defined(__cplusplus)
extern "C" {
#endif

/**
*/
void kaapi_abort(unsigned long int line, const char* file, const char* msg)
{
  if (msg)
    printf("**** %s. Line: %li, File: '%s'\n", msg, line, file);
  else
    printf("**** Aborting Line: %li, File: '%s'\n", line, file);
  abort();
}

/* autotools can prompt for this ... */
const char* get_kaapi_version(void)
{ return "Git last commit:" GIT_HASH; }


/* autotools can prompt for this ... */
const char* get_kaapi_git_hash(void)
{ return GIT_HASH; }


#if defined(__cplusplus)
}
#endif

