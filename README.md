# Welcome to the libKOMP repository

libKOMP is based on [libomp](http://llvm.org/git/openmp),
the Intel OpenMP runtime for icc/ifort and clang compilers.
The current version is based on branch release_39.

We extend the origin libomp runtime to include some missing 
features or to add algorithms provided by [KAAPI](http://kaapi.gforge.inria.fr) 
such as work stealing protocol, task scheduling or loop scheduling. 
  Our goal was to produce a robust OpenMP library with very 
low overhead in task scheduling and loop scheduling for a 
wide range of applications and architectures, 
including NUMA architecture and many-core such as Intel Xeon Phi.

### In this version 
The current extensions include:
* T.H.E. protocol for work stealing
This protocol comes from Cilk it gives priority to victim thread in order to make progress. Victim and thief are concurrent except in rare case where they are serialized.
* request combining
Thanks to the T.H.E. serialization, the thieves make coordination in order to steal together a victim.
* task affinity scheduler
On NUMA architecture it is important to control affinity between task and data. The affinity scheduler is both a scheduling algorithm and a compiler extension to provide specifc clause for the OpenMP task directive. It is also possibile to make OpenMP runtime function call.
* support for GOMP dependent task
Feature is missing in the original libomp.


### Quick start
Please read the quick configuration and installation 
guide [here](https://gitlab.inria.fr/openmp/libkomp/wikis/getting-started).

### The authors
Thierry Gautier, INRIA
Philippe Virouleau, INRIA

#### Acknowledgment 
This project is partially funded by the french PIA ELCI project.
